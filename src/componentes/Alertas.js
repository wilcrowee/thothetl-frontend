
export default function AlertaErro({msg, setMsg}){
    const limparErro = () => {
        setMsg('');
    };

    return(
        <div className="alert alert-danger alert-dismissible fade show" role="alert">
            Que Pena! {msg}
            <button onClick={limparErro} type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    );
}

export function AlertaSucesso({msg, setMsg}){
    const limparAlerta = () => {
        setMsg('');
    };

    return(
        <div className="alert alert-success alert-dismissible fade show" role="alert">
            Tudo Certo! {msg}
            <button onClick={limparAlerta} type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    );
}