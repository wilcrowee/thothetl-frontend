import React, { useState } from 'react';

export default function Login(props){
    const [email, setEmail] = useState("");
    const [senha, setSenha] = useState("");
    const [senha2, setSenha2] = useState("");
    const [usuario, setUsuario] = useState("");
    
    function enviarRegistro(e) {
        e.preventDefault();

        if (senha === senha2){
            const csrfToken = props.csrfToken;
            let formData = new FormData();
            formData.append('email', email);
            formData.append('password', senha);
            formData.append('password2', senha2);
            formData.append('username', usuario);
    
            const config = {
                headers: {
                    'X-CSRFToken': csrfToken,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
    
            props.client.post(
                "/api/register",
                formData,
                config
            ).then(function(res) {
                const csrfToken = props.csrfToken;
                formData = new FormData();
                formData.append('email', email);
                formData.append('password', senha);
                
                const config = {
                    headers: {
                        'X-CSRFToken': csrfToken,
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                };

                props.client.post(
                "/api/login",
                formData,
                config
                ).then(function(res) {
                    props.setCurrentUser(true);
                    props.setRegistrationToggle(false);
                });
            }).catch(error => {
                if (error.response) {
                    const elemento = document.getElementById("login_erro");
                    const senhaInput = document.getElementById("senha");
                    const senha2Input = document.getElementById("senha2");
                    const emailInput = document.getElementById("email");
                    const usuarioInput = document.getElementById("usuario");
                    
                    if (error.response.data.campo === 'email'){
                        elemento.innerHTML = `
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Que pena!</strong> Email já cadastrado.
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        `;

                        senhaInput.classList.remove('is-invalid');
                        senha2Input.classList.remove('is-invalid');
                        emailInput.classList.add('is-invalid');
                        usuarioInput.classList.remove('is-invalid');
                    }else{
                        elemento.innerHTML = `
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Que pena!</strong> Nome de usuário já cadastrado.
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        `;

                        senhaInput.classList.remove('is-invalid');
                        senha2Input.classList.remove('is-invalid');
                        emailInput.classList.remove('is-invalid');
                        usuarioInput.classList.add('is-invalid');
                    }
                } else if (error.request) {
                    // A solicitação foi feita, mas não houve resposta do servidor
                    console.error('Sem resposta do servidor:', error.request);
                } else {
                    // Ocorreu um erro durante o processo de solicitação
                    console.error('Erro ao processar a solicitação:', error.message);
                }
            });
        }else{
            const elemento = document.getElementById("login_erro");
            const senhaInput = document.getElementById("senha");
            const senha2Input = document.getElementById("senha2");
            const emailInput = document.getElementById("email");
            const usuarioInput = document.getElementById("usuario");

            elemento.innerHTML = `
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Que pena!</strong> Email ou senha inválidos.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            `;

            senhaInput.classList.add('is-invalid');
            senha2Input.classList.add('is-invalid');
            emailInput.classList.remove('is-invalid');
            usuarioInput.classList.remove('is-invalid');
        }        
    }

    function enviarLogin(e) {
        e.preventDefault();
        const csrfToken = props.csrfToken;
        const formData = new FormData();
        formData.append('email', email);
        formData.append('password', senha);

        const config = {
            headers: {
                'X-CSRFToken': csrfToken,
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        props.client.post(
            "/api/login",
            formData,
            config
        ).then(function(res) {
                props.setCurrentUser(true);
                props.setCarregando(true);
                window.location.reload();
        }).catch(error => {
            if (error.response) {
                if (error.response.status === 400){
                    const elemento = document.getElementById("login_erro");
                    const emailInput = document.getElementById("email");
                    const senhaInput = document.getElementById("senha");

                    elemento.innerHTML = `
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Que pena!</strong> Email ou senha inválidos.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    `;
                    emailInput.classList.add('is-invalid');
                    senhaInput.classList.add('is-invalid');
                }
            } else if (error.request) {
                console.log('Sem resposta do servidor:', error.request);
            } else {
                console.log('Erro ao processar a solicitação:', error.message);
            }
        });
    }
    
    if(!props.currentUser){
        if(!props.registrationToggle){
            return(
                <main id="main" className="main">
                    <div>
                        <div className="pagetitle">
                            <h1 id="tituloPagina">Login</h1>
                            <nav id="navTitulo"> 
                            </nav>
                        </div>
                        <section className="section">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="card">
                                        <div className="card-body">
                                            <div id="login_erro"></div>
                                            <form onSubmit={e => enviarLogin(e)}>
                                                <div id="formulario">
                                                    <div className="row mb-3">
                                                        <label htmlFor="email" className="col-sm-3 col-form-label">Email</label>
                                                        <div className="col-sm-9">
                                                            <input id="email" type="email" className="form-control" value={email} onChange={e => setEmail(e.target.value)} required/>
                                                        </div>
                                                    </div>
                                                    <div className="row mb-3">
                                                        <label htmlFor="senha" className="col-sm-3 col-form-label">Senha</label>
                                                        <div className="col-sm-9">
                                                            <input id="senha" type="password" className="form-control" value={senha} onChange={e => setSenha(e.target.value)} required />
                                                        </div>
                                                    </div>
                                                    <button type="submit" className="btn btn-primary">Enviar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </main>
            );
        }else{
            return(
                <main id="main" className="main">
                    <div>
                        <div className="pagetitle">
                            <h1 id="tituloPagina">Registro</h1>
                            <nav id="navTitulo"> 
                            </nav>
                        </div>
                        <section className="section">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div id="login_erro"></div>
                                    <div className="card">
                                        <div className="card-body">
                                            <div id="login_erro"></div>
                                            <form onSubmit={e => enviarRegistro(e)}>
                                                <div id="formulario">
                                                    <div className="row mb-3">
                                                        <label htmlFor="email" className="col-sm-3 col-form-label">Email</label>
                                                        <div className="col-sm-9">
                                                            <input id="email" type="email" className="form-control" value={email} onChange={e => setEmail(e.target.value)} required />
                                                        </div>
                                                    </div>
                                                    <div className="row mb-3">
                                                        <label htmlFor="senha" className="col-sm-3 col-form-label">Usuário</label>
                                                        <div className="col-sm-9">
                                                            <input id="usuario" type="text" className="form-control" value={usuario} onChange={e => setUsuario(e.target.value)} required />
                                                        </div>
                                                    </div>
                                                    <div className="row mb-3">
                                                        <label htmlFor="senha" className="col-sm-3 col-form-label">Senha</label>
                                                        <div className="col-sm-9">
                                                            <input id="senha" type="password" className="form-control" value={senha} onChange={e => setSenha(e.target.value)} required />
                                                        </div>
                                                    </div>
                                                    <div className="row mb-3">
                                                        <label htmlFor="senha2" className="col-sm-3 col-form-label">Confirme a Senha</label>
                                                        <div className="col-sm-9">
                                                            <input id="senha2" type="password" className="form-control" value={senha2} onChange={e => setSenha2(e.target.value)} required />
                                                        </div>
                                                    </div>
                                                    <button type="submit" className="btn btn-primary">Enviar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </main>
            );
        }
    }
}