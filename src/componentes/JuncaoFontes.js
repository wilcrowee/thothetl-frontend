import React, {useState, useEffect, useRef, useCallback} from "react";
import { useDrag, useDrop } from 'react-dnd';
import { removerColuna } from "./DataFrame";
import { tecnologia } from './Valores';
import Carregando from './Carregando'
import update from 'immutability-helper';
import moment from 'moment'


const DraggableBox = ({ fonte, onDropItem }) => {
  const [{ isDragging }, drag] = useDrag(() => ({
    type: 'BOX',
    item: { id: fonte.id, inBox: fonte.inBox },
    end: (item, monitor) => {
      if (!monitor.didDrop()) {
        onDropItem(item.id, item.inBox ? false : true); // Toggle inBox state
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  }));
  
  return (
    <div ref={drag} className="card" style={{ opacity: isDragging ? 0.5 : 1 }}>
      <div className="row g-0">
        <div className="col-md-4 carrossel-img">
          <img src={tecnologia[fonte.tipo][fonte.tecnologia]?.imagem} className="img-fluid rounded-start" alt="..." />
        </div>
        <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title">{fonte.tipo === 0 ?("Banco:"):("Arquivo")} {fonte.banco}</h5>
            <p className="card-text">{tecnologia[fonte.tipo][fonte.tecnologia]?.nome}</p>
            Criação: {moment(fonte.data_criacao).format('DD/MM/YYYY HH:mm:ss')}
          </div>
        </div>
      </div>
    </div>
  );
};
  
const DropTarget = ({ onDrop, children}) => {
    const [{ isOver, canDrop }, drop] = useDrop(() => ({
        accept: 'BOX',
        drop: (item, monitor) => {
            if (monitor.didDrop()) return;
            onDrop(item.id, true); // Set inBox to true
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    }));
    
    const backgroundColor = isOver && canDrop ? '#017b6fa8' : 'white';

    return (
        <div className="col-md-6" ref={drop} style={{ backgroundColor }}>
            <div className="juncao_area">
                <div className="row">
                    {children}
                </div>
            </div>
        </div>
    );
};

const SortableItem = ({ text, index, moveItem }) => {
  const ref = useRef(null);

  const [, drop] = useDrop({
      accept: 'SORTABLE_ITEM',
      hover(item, monitor) {
          if (!ref.current) {
              return;
          }
          const dragIndex = item.index;
          const hoverIndex = index;

          if (dragIndex === hoverIndex) {
              return;
          }

          const hoverBoundingRect = ref.current.getBoundingClientRect();
          const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
          const clientOffset = monitor.getClientOffset();
          const hoverClientY = clientOffset.y - hoverBoundingRect.top;

          if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
              return;
          }

          if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
              return;
          }

          moveItem(dragIndex, hoverIndex);
          item.index = hoverIndex;
      },
  });

  const [{ isDragging }, drag] = useDrag({
      type: 'SORTABLE_ITEM',
      item: { index },
      collect: (monitor) => ({
          isDragging: monitor.isDragging(),
      }),
  });

  drag(drop(ref));

  return (
      <div ref={ref} className="relative flex space-x-3 border rounded p-2 bg-gray-100">
          {text}
      </div>
  );
};

const SortableList = ({lista, setLista}) => {
  const [items, setItems] = useState(lista);

  const moveItem = useCallback((dragIndex, hoverIndex) => {
      const draggedItem = items[dragIndex];
      const updatedItems = update(items, {
        $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, draggedItem],
        ],
      })

      setItems(updatedItems);
      setLista(updatedItems);
  }, [items]);

  return (
      <div>
          {items.map((item, index) => (
              <SortableItem
                  key={index}
                  index={index}
                  text={item}
                  moveItem={moveItem}
              />
          ))}
      </div>
  );
};
  
export default function JuncaoFontes({csrfToken, client, projeto_id, setJoin}) {
    const [items, setItems] = useState([]);
    const [selecionados, setSelecionados] = useState(0);
    const [fontePrincipal, setFontePrincipal] = useState([]);
    const [fonteSecundaria, setFonteSecundaria] = useState([]);
    const [listaPrincipal, setListaPrincipal] = useState([]); 
    const [listaSecundaria, setListaSecundaria] = useState([]);
    const [selectPrincipal, setSelectPrincipal] = useState('');
    const [selectSecundario, setSelectSecundario] = useState('');
    const [nomes, setNomes] = useState([]);    
    const [verifica, setVerifica] = useState(false);
    const [loading, setLoading] = useState(false);
    const [ids, setIds] = useState([]);
    const config = {
      headers: {
        'X-CSRFToken': csrfToken,
      }
    };

    useEffect(() => {
      const formData = new FormData();
      formData.append('projeto_id', projeto_id);
            
      client.post(
        "/pincel/fonte",
        formData,
        config
      ).then(function(res) {
        let fontesMerged = [];
        const retorno = res.data.data;
        const fontes = retorno.map(item => ({
          ...item,
          inBox: false
        }));

        fontes.forEach((item) => {
          if (item.tipo === 2) {
            let aux = JSON.parse(item.banco);
            // Presumindo que aux é um array de IDs
            fontesMerged.push(...aux);
          } 
        });

        // Extrair IDs diretamente (já são IDs)
        const idsToRemove = fontesMerged;
        const fontesFiltradas = fontes.filter(item => !idsToRemove.includes(item.id));
        
        setItems(fontesFiltradas);                      
      });            
    }, []);
  
    useEffect(() => {
      const itemsInBox = items.filter(item => item.inBox);
      setSelecionados(itemsInBox.length);
      let aux = 0;
      let fonteIDs = [];
          
      if (itemsInBox.length === 2){
        itemsInBox.map((item) => {
          let dataFrame = [];
          const formData = new FormData();
          formData.append('fonte_id', item.id);
          
          client.post(
            "/pincel/extracao",
            formData,
            config
          ).then(function(res) {
            dataFrame = res.data.data;
            console.log("Datframe");
            console.log(dataFrame);
            client.get(
              "/pincel/alteracao/"+item.id+"/",
              config
            ).then(function(res) {
              res.data.data.forEach(item => {
                if (item.tipo === 3){
                  let valor = JSON.parse(item.valor);
                  if (valor[0] === 0){
                    dataFrame = removerColuna(dataFrame, valor[1]);
                  }else if (valor[0] === 1){ //Remover Linha
                    dataFrame = dataFrame.filter((_, index) => index !== valor[1]);
                  }
                }
                if (item.tipo === 2){
                  let valor = JSON.parse(item.valor);
                  if (valor[0] === 0){
                    dataFrame[0][valor[1]] = valor[2];
                  }else{
                    dataFrame[valor[1]][valor[2]] = valor[3];
                  }
                }
              });
              if(aux === 0){
                setFontePrincipal(dataFrame);
                setListaPrincipal(dataFrame[0]);
                fonteIDs.push(item.id);
                aux = 1;
              }else{
                setFonteSecundaria(dataFrame);
                setListaSecundaria(dataFrame[0]);
                fonteIDs.push(item.id);
              }
            });
          });
        })
      }

      setIds(fonteIDs);
    }, [items]);

    useEffect(() => {
      const itemsInBoxCount = items.filter((item) => item.inBox);

      if (itemsInBoxCount.length === 2){
        setVerifica(false);
        if(fontePrincipal.length !== 0 && fonteSecundaria.length !== 0){
          if(fontePrincipal[0].length === fonteSecundaria[0].length){
            setVerifica(true);
          }
        }
      }

    }, [fontePrincipal, fonteSecundaria]);
    
    const handleDropItem = (id, inBox) => {
      setItems((prevItems) => {
        const itemsInBoxCount = prevItems.filter((item) => item.inBox).length;
        
        if (inBox && itemsInBoxCount >= 2) {
          return prevItems; 
        }

        return prevItems.map((item) =>
          item.id === id ? { ...item, inBox } : item
        );
      });
    };

    function enviaHorizontal(){
      const formData = new FormData();
      formData.append('relacao', JSON.stringify([selectPrincipal, selectSecundario]));
      formData.append('ids', JSON.stringify(ids));
      formData.append('projeto_id', projeto_id);

      setLoading(true); // Mostra a tela de carregamento
      
      client.post(
        "/pincel/horizontal",
        formData,
        config,
        { headers: { "Content-Type": "application/x-www-form-urlencoded" }}
      ).then(function(res) {
        setListaSecundaria([]);
        setFontePrincipal(res.data.data);
        setLoading(false); // Esconde a tela de carregamento
        setJoin(false);
      })     
    }

    function enviaVertical(){
      const formData = new FormData();
      formData.append('principal', JSON.stringify(listaPrincipal));
      formData.append('secundario', JSON.stringify(listaSecundaria));
      formData.append('fontePrincipal', JSON.stringify(fontePrincipal));
      formData.append('fonteSecundaria', JSON.stringify(fonteSecundaria));
      formData.append('ids', JSON.stringify(ids));
      formData.append('projeto_id', projeto_id);
      
      setLoading(true); // Mostra a tela de carregamento
      
      client.post(
        "/pincel/vertical",
        formData,
        config,
        { headers: { "Content-Type": "application/x-www-form-urlencoded" }}
      ).then(function(res) {
        setListaSecundaria([]);
        setFontePrincipal(res.data.data);
      })
      
      setLoading(false); // Esconde a tela de carregamento
      setJoin(false);
    }

    function cancelarJoin(){
      setJoin(false);
    }

    const selectPrincipalChange = (event) => {
      setSelectPrincipal(event.target.value);
    };
  
    const selectSecundarioChange = (event) => {
      setSelectSecundario(event.target.value);
    };

    return (
      <div className="fontes">
        <button className="tab-join" onClick={cancelarJoin}>x</button>
        <p className="fonte_titulo">Junção de Dados</p>
        <div className="row juncao_fonte">
          {items.filter(item => !item.inBox).map((item) => (
            <div className="col-md-3 juncao_item" key={item.id}>
              <DraggableBox
                fonte={item}
                onDropItem={handleDropItem}
              />
            </div>
          ))}
        </div>
        <div className="row">
          <DropTarget onDrop={handleDropItem}>
            {items.filter(item => item.inBox).map((item) => (
              <div className="col-md-6" key={item.id}>
                <DraggableBox
                  fonte={item}
                  onDropItem={handleDropItem}
                />
              </div>
            ))}
          </DropTarget>
          {selecionados === 2 &&(
          <div className="col-md-6">
              <ul className="nav nav-tabs d-flex" id="merge" role="tablist">
                <li className="nav-item flex-fill" role="presentation">
                  <button className="nav-link w-100 active" id="vertical-tab" data-bs-toggle="tab" data-bs-target="#vertical" type="button" role="tab" aria-controls="vertical" aria-selected="true">vertical</button>
                </li>
                <li className="nav-item flex-fill" role="presentation">
                  <button className="nav-link w-100" id="horizontal-tab" data-bs-toggle="tab" data-bs-target="#horizontal" type="button" role="tab" aria-controls="horizontal" aria-selected="false" tabIndex="-1">Horizontal</button>
                </li>
              </ul>
              <div className="tab-content pt-2" id="mergeContent">
                <div className="tab-pane fade active show" id="vertical" role="tabpanel" aria-labelledby="vertical-tab">
                  <div className="card-body row">
                    <p className="justificado">A junção de dados vertical, também conhecida como empilhamento ou concatenacão, é uma operação na qual duas ou mais tabelas são combinadas em uma única tabela, adicionando linhas uma após a outra. Isso é útil quando você tem tabelas com a mesma estrutura de colunas, mas diferentes conjuntos de linhas e deseja combiná-las em uma única tabela.</p>
                    <div className="row">
                      <div className="col-md-2 icon">
                        <i className="bi bi-lightbulb"></i>
                      </div>
                      <p className="col-md-10 justificado">Ao realizar uma junção de dados vertical, você obterá uma única tabela combinando todas as linhas das duas tabelas</p>
                    </div>
                    {verifica ?(
                    <div className="row">
                      <p>Ordene os campos conforme as suas correspondencias:</p>
                      <br />
                      <div className="row">
                        <main className="col-md-6">
                          <p className="text-xl font-bold mt-4"><strong>1ª Fonte:</strong></p>
                          <SortableList lista={listaPrincipal} setLista={setListaPrincipal} /> 
                        </main>
                        <main className="col-md-6">
                          <p className="text-xl font-bold mt-4"><strong>2ª Fonte:</strong></p>
                          <SortableList lista={listaSecundaria} setLista={setListaSecundaria} /> 
                        </main>
                      </div>
                      <div className="row">
                        <div className="card-body mb-3 col-sm-3">
                          <button className="btn btn-primary" onClick={enviaVertical}>Enviar Selecionados</button>
                        </div>
                      </div>
                    </div>
                    ):(
                    <div className="card-body row">
                      <p className="justificado">Esta operação não pode ser feita, pois a quantidade de atributos entre os dados anteriores e os dados atuais são diferentes.</p>
                    </div>
                    )}
                  </div>
                </div>
                <div className="tab-pane fade" id="horizontal" role="tabpanel" aria-labelledby="horizontal-tab">
                  <div className="card-body row">
                    <p className="justificado">A junção horizontal, também conhecida como concatenação de colunas ou adição de colunas, é uma operação onde você combina duas ou mais tabelas colocando as colunas lado a lado. Isso é útil quando você tem tabelas com diferentes conjuntos de colunas e deseja combiná-las em uma única tabela.</p>  
                    <br />
                    <div className="row col-sm-12">
                      <p>Ordene os campos conforme as suas correspondencias:</p>
                      <br />
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-3">
                        <label className="col-form-label">1ª Fonte:</label>
                      </div>
                      <div className="col-sm-9">
                        <select id="fontePrincipal" className="form-select" value={selectPrincipal} onChange={selectPrincipalChange}>
                        <option value="">Selecione uma variavel.</option>
                        {fontePrincipal.length > 0 &&(
                        listaPrincipal.map((coluna, index) => (
                        <option key={index} value={coluna}>{coluna}</option>
                        ))
                        )}                        
                        </select>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-3">
                        <label className="col-form-label">2ª Fonte:</label>
                      </div>
                      <div className="col-sm-9">
                        <select id="fonteSecundaria" className="form-select" value={selectSecundario} onChange={selectSecundarioChange}>
                        <option value="">Selecione uma variavel.</option>
                        {fonteSecundaria.length > 0  &&(
                        listaSecundaria.map((coluna, index) => (
                        <option key={index} value={coluna}>{coluna}</option>
                        ))
                        )}
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="card-body mb-3 col-sm-3">
                      <button className="btn btn-primary" onClick={enviaHorizontal}>Enviar Selecionados</button>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          )}
        </div>
        {loading && <Carregando />}
      </div>
    );
  }