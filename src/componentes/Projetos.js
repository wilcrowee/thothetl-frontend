import React, { useState, useEffect, useCallback, useRef, useLayoutEffect } from "react";
import { Pagination, Navigation } from 'swiper/modules';
import { ContextMenu } from 'primereact/contextmenu';
import { Swiper, SwiperSlide } from 'swiper/react';
import { ModalEditarProjeto, ModalEditarFonte, ModalDeletarFonte, ModalEditarBanco, ModalBanco } from "./Modal";
import JuncaoFontes from './JuncaoFontes'
import Tabela from "./DataFrame";
import { tecnologia } from './Valores';
import moment from 'moment';

import 'primereact/resources/themes/arya-blue/theme.css';
import 'primeicons/primeicons.css';
import 'swiper/css';
import AlertaErro from "./Alertas";
import NormalizarNota from "./Normalizar";

export default function Projeto(props){
    const csrfToken = props.csrfToken;
    const [fonte, setFonte] = useState({id: 0, tipo: 0, tecnologia: 0, banco: "", data_criacao: ""});
    const [temFonte, setTemFonte] = useState(false);
    const [msg, setMsg] = useState('');
    const [isUpdated, setIsUpdated] = useState(false);
    
    function fecharProjeto(){
        props.setProjeto({id: '', nome: '', descricao: ''});
        props.setProjetoAtivo(false);
        setTemFonte(false);
        sessionStorage.removeItem('projeto');
        sessionStorage.removeItem('conexao');
        sessionStorage.removeItem('bancos'); 
    }

    useEffect(() => {
        setMsg("");
        setFonte({id: 0, tipo: 0, tecnologia: 0, banco: "", data_criacao: ""});
        props.setComando("");
        props.setEtapa(0);        
    },[props.projeto]);


    useEffect(() => {
        setFonte({id: 0, tipo: 0, tecnologia: 0, banco: "", data_criacao: ""});
        setTemFonte(false); 
    },[props.etapa]);

    useEffect(() => {
        setIsUpdated(true);
    }, [fonte, temFonte]);

    useEffect(() => {
        if (isUpdated && props.comando === "Normalizar_Nota" && fonte.id === 0) {
            setMsg("Nenhuma Fonte foi selecionada");
            props.setComando("");
        }
    }, [props.comando, isUpdated, fonte.id]);



    return(
        <div>
            <main id="main" className="main">
                <div>
                    <div className="pagetitle">
                        <h1 id="tituloPagina">Projeto Ativo</h1>
                        <nav id="navTitulo"> 
                        </nav>
                    </div>
                    <section className="section">
                        <div className="row">
                            <div className="col-lg-12">
                                <ul className="nav nav-tabs" id="myTab" role="tablist">
                                    <li className="nav-item" role="presentation">
                                        <span className="nav-link active" type="button" role="tab" aria-selected="true">
                                            {props.projeto.nome}
                                            <button className="tab-close-button tab-projeto" onClick={fecharProjeto}>x</button>
                                        </span>
                                    </li>
                                </ul>
                                <div className="card">
                                    <div className="card-body">
                                        <Pipeline etapa={props.etapa} />
                                        {msg &&(
                                            <AlertaErro msg={msg} setMsg={setMsg} />
                                        )}
                                        {props.etapa === 0 &&(
                                            <>
                                            <CarrosselConexao 
                                                csrfToken={csrfToken} 
                                                client={props.client} 
                                                projeto_id={props.projeto.id}
                                                conexoes={props.conexoes}
                                                setConexoes={props.setConexoes}
                                                setCarregando={props.setCarregando}
                                                setFonte={setFonte}
                                                setTemFonte={setTemFonte}
                                            />
                                            {fonte.id !== 0 &&(
                                                <ApresentaFonte  
                                                    csrfToken={csrfToken} 
                                                    client={props.client} 
                                                    fonte={fonte}
                                                    setFonte={setFonte}
                                                />
                                            )}
                                            </>
                                        )}
                                        {props.etapa === 1 &&(
                                            props.comando === "Normalizar_Nota" ? (
                                                <>
                                                    <CarrosselConexao 
                                                        csrfToken={csrfToken} 
                                                        client={props.client} 
                                                        projeto_id={props.projeto.id}
                                                        conexoes={props.conexoes}
                                                        etapa={1}
                                                        setConexoes={props.setConexoes}
                                                        setCarregando={props.setCarregando}
                                                        setFonte={setFonte}
                                                        setTemFonte={setTemFonte}
                                                    />
                                                    <NormalizarNota  
                                                        csrfToken={csrfToken} 
                                                        client={props.client}
                                                        fonte={fonte} 
                                                        setComando={props.setComando}
                                                        setMsg={setMsg}
                                                    />
                                                </>
                                            ) : props.join ? (
                                                <JuncaoFontes 
                                                    csrfToken={csrfToken} 
                                                    client={props.client} 
                                                    projeto_id={props.projeto.id}
                                                    setJoin={props.setJoin}
                                                />
                                            ) : (
                                                <>
                                                <CarrosselConexao 
                                                    csrfToken={csrfToken} 
                                                    client={props.client} 
                                                    projeto_id={props.projeto.id}
                                                    conexoes={props.conexoes}
                                                    etapa={1}
                                                    setConexoes={props.setConexoes}
                                                    setCarregando={props.setCarregando}
                                                    setFonte={setFonte}
                                                    setTemFonte={setTemFonte}

                                                />
                                                {fonte.id !== 0 &&(
                                                <ApresentaFonte  
                                                    csrfToken={csrfToken} 
                                                    client={props.client} 
                                                    fonte={fonte}
                                                    setFonte={setFonte}
                                                />
                                                )}
                                                </>
                                            )
                                        )}
                                        <Navegacao  temFonte={temFonte} etapa={props.etapa} setEtapa={props.setEtapa} />                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </main>
        </div>
    );
}

export function SelecionaProjetos(props){
    const csrfToken = props.csrfToken;
    const [projetos, setProjetos] = useState([]);
    const [recarregar, setRecarregar] = useState(false);
    
    const obterProjetos = useCallback(() => {
        const config = {
            headers: {
                'X-CSRFToken': csrfToken,
            }
        };

        props.client.get(
            "/pincel/projeto",
            config
        ).then(function(res) {
            const novosProjetos = res.data.data.map(projeto => {
                return {
                    id: projeto[0], 
                    nome: projeto[1],
                    descricao: projeto[2],
                    data: moment(projeto[3]).format('DD/MM/YYYY HH:mm:ss'),
                };
            });

            setProjetos(novosProjetos);                           
        });
    }, [props.client, csrfToken]);

    useEffect(() => {
        obterProjetos();
        setRecarregar(false);
    }, [obterProjetos, recarregar]); 

    const selecionarProjeto = (id) => {
        const projeto = projetos.find(projeto => projeto.id === id)

        props.setProjetoAtivo(true);
        props.setProjeto(projeto);
        sessionStorage.setItem('projeto', JSON.stringify(projeto));
        props.setSelecionando('');
    }

    const deletarProjeto = (id) => {
        const config = {
            headers: {
                'X-CSRFToken': csrfToken,
            }
        };
    
        props.client.delete(
            "/pincel/projeto/"+id,
            config
        ).then(function(res) {
            obterProjetos();
            const storedData = sessionStorage.getItem('projeto');

            if (storedData) {
                const projeto_sessao = JSON.parse(storedData);
                
                if (projeto_sessao.id === id){
                    props.setProjetoAtivo(false);
                    props.setProjeto({id: '', nome: '', descricao: ''});
                    sessionStorage.removeItem('projeto');
                    props.setSelecionando('');
                }
            }  
        });
    }

    return(
        <main id="main" className="main">
            <div>
                <div className="pagetitle">
                    <h1 id="tituloPagina">Selecionar Projeto</h1>
                    <nav id="navTitulo"> 
                    </nav>
                </div>
                <section className="section">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <table className="table table-hover">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nome</th>
                                                <th scope="col">Descrição</th>
                                                <th scope="col">Criação</th>
                                                <th scope="col">Abrir</th>
                                                <th scope="col">Editar</th>
                                                <th scope="col">Deletar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {projetos.map((item, index) => (
                                            <tr key={index}>
                                                <td>{item.nome}</td>
                                                <td>{item.descricao}</td>
                                                <td>{item.data}</td>
                                                <td><button onClick={() => selecionarProjeto(item.id)}type="button" className="btn btn-primary"><i className="bi bi-app-indicator"></i></button></td>
                                                <td><button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target={"#modalEditaProjeto"+item.id}><i className="bi bi-pencil-square"></i></button></td>
                                                <td><button onClick={() => deletarProjeto(item.id)} type="button" className="btn btn-primary"><i className="bi bi-trash-fill"></i></button></td>
                                            </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div>
                {projetos.map((item, index) => (
                    <ModalEditarProjeto key={index} client={props.client} csrfToken={props.csrfToken} projeto={item} setRecarregar={setRecarregar} setProjeto={props.setProjeto} />
                ))}
            </div>
        </main>
    );
}

function Pipeline(props){
    
    return(
        <div className="pipeline">
            <p>Ciclo de Vida ETL</p>
            {props.etapa === 0 &&(
                <div className="row d-flex">
                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Extract_Active.png" alt="Imagem_Extract_Ativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Arrow_Inactive.png" alt="Imagem_Seta_Inativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Transform_Inactive.png" alt="Imagem_Transform_Inativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                    <img src="/assets/img/pipeline/Arrow_Inactive.png" alt="Imagem_Seta_Inativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Load_Inactive.png" alt="Imagem_Load_Inativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-1"></div>
                </div>   
            )}
            {props.etapa === 1 &&(
                <div className="row d-flex">
                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Extract_Active.png" alt="Imagem_Extract_Ativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Arrow_Active.png" alt="Imagem_Seta_Ativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Transform_Active.png" alt="Imagem_Transform_Ativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Arrow_Inactive.png" alt="Imagem_Seta_Inativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-2">
                        <img src="/assets/img/pipeline/Load_Inactive.png" alt="Imagem_Load_Inativa" className="img-fluid imagem" />
                    </div>
                    <div className="col-md-1"></div>
                </div>    
            )}
            {props.etapa === 2 &&(
                <div className="row d-flex">
                <div className="col-md-1"></div>
                <div className="col-md-2">
                    <img src="/assets/img/pipeline/Extract_Active.png" alt="Imagem_Extract_Ativa" className="img-fluid imagem" />
                </div>
                <div className="col-md-2">
                    <img src="/assets/img/pipeline/Arrow_Active.png" alt="Imagem_Seta_Ativa" className="img-fluid imagem" />
                </div>
                <div className="col-md-2">
                    <img src="/assets/img/pipeline/Transform_Active.png" alt="Imagem_Transform_Ativa" className="img-fluid imagem" />
                </div>
                <div className="col-md-2">
                <img src="/assets/img/pipeline/Arrow_Active.png" alt="Imagem_Seta_Ativa" className="img-fluid imagem" />
                </div>
                <div className="col-md-2">
                    <img src="/assets/img/pipeline/Load_Active.png" alt="Imagem_Load_Ativa" className="img-fluid imagem" />
                </div>
                <div className="col-md-1"></div>
            </div>    
            )}
        </div>
    );
}

function CarrosselConexao(props){
    const csrfToken = props.csrfToken;
    const contextRef = useRef(null);
    const contextRefArquivo = useRef(null);
    const contextRefMerge = useRef(null);
    const editarBancoRef = useRef(null);
    const editarRef = useRef(null);
    const deletarRef = useRef(null);
    const [conexoes, setConexoes] = useState([]);
    const [index, setIndex] = useState(0);
    const [selecionando, setSelecionando] = useState(false);
    const config = {
        headers: {
            'X-CSRFToken': csrfToken,
        }
    };

    useEffect(() => {
        if (props.conexoes.length !== 0){
            setConexoes(props.conexoes);
        }else{
            const formData = new FormData();
            formData.append('projeto_id', props.projeto_id);
            
            props.client.post(
                "/pincel/fonte",
                formData,
                config
            ).then(function(res) {
                let retorno = res.data.data;
                let remover = [];

                if (!props.etapa) {
                    retorno = retorno.filter(item => item.tipo !== 2);
                }else{
                    retorno.map(item => {
                        if (item.tipo === 2){
                            try {
                                let aux = JSON.parse(item.banco);
                                remover.push(aux[0]);
                                remover.push(aux[1]);
                            } catch (e) {
                                console.error('Erro ao parsear item:', item, e);
                            }
                        }
                    });
                  
                    retorno = retorno.filter(objeto => !remover.includes(objeto.id));
                }

                setConexoes(retorno);

                if (res.data.data && res.data.data.length > 0){
                    props.setTemFonte(true);
                }                         
            });    
        }        
    }, [props.conexoes]);

    const editarBanco = () => {
        editarBancoRef.current.click();
    }

    const editarFonte = () => {
        editarRef.current.click();
    }

    const deletarFonte = () => {
        deletarRef.current.click();
    }

    const handleRowContextMenu = (e, index, tipo) => {
        e.preventDefault();
        if(tipo === 0){
            contextRef.current.show(e);
            contextRefArquivo.current.hide(e);
            contextRefMerge.current.hide(e);
        }else if(tipo === 1){
            contextRef.current.hide(e);
            contextRefArquivo.current.show(e);
            contextRefMerge.current.hide(e);
        }else{
            contextRef.current.hide(e);
            contextRefArquivo.current.hide(e);
            contextRefMerge.current.show(e);
        }
        setIndex(index);
    };

    const atualizarConexoes = (novasConexoes) => {
        setConexoes(novasConexoes);
    };

    const apresentaFonte = (index) => {
        props.setFonte(conexoes[index]);
    };
    
    const items = [
        { 
            label: 'Editar Banco',
            icon: 'pi pi-pen-to-square', 
            command: editarBanco
        },
        { 
            label: 'Editar Fonte',
            icon: 'pi pi-file-edit', 
            command: editarFonte
        },
        { 
            label: 'Deletar Fonte', 
            icon: 'pi pi-times',
            command: deletarFonte 
        }
    ];

    const items_arquivo = [
        { 
            label: 'Editar Fonte',
            icon: 'pi pi-file-edit', 
            command: editarFonte
        },
        { 
            label: 'Deletar Fonte', 
            icon: 'pi pi-times',
            command: deletarFonte 
        }
    ];

    const items_merge = [
        { 
            label: 'Deletar Fonte', 
            icon: 'pi pi-times',
            command: deletarFonte 
        }
    ];
    
    return(
        <div className="fontes">
            <p className="fonte_titulo">Fontes de Dados</p>
            <Swiper
                spaceBetween={10}
                slidesPerView={4}
                pagination={{
                    type: 'fraction',
                }}
                navigation={true}
                modules={[Pagination, Navigation]}
                className="carrossel"
            >   
                {conexoes.map((item, index) => (   
                <SwiperSlide key={index} onClick={() => apresentaFonte(index)}>
                <div className="card" onContextMenu={(e) => handleRowContextMenu(e, index, item.tipo)}>
                    <div className="row g-0">
                        <div className="col-md-4 carrossel-img">
                            <img src={tecnologia[item.tipo][item.tecnologia]?.imagem} className="img-fluid rounded-start" alt="..." />
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                <h5 className="card-title">{item.tipo === 0 ?("Banco:"):(item.tipo === 1 ?("Arquivo:"):("Fonte: "))} {item.banco}</h5>
                                <p className="card-text">{tecnologia[item.tipo][item.tecnologia]?.nome}</p>
                                Criação: {moment(item.data_criacao).format('DD/MM/YYYY HH:mm:ss')}
                            </div>
                        </div>
                    </div>
                </div>
                </SwiperSlide> 
                ))}
            </Swiper>
            <ContextMenu model={items} ref={contextRef} breakpoint="767px" />
            <ContextMenu model={items_arquivo} ref={contextRefArquivo} breakpoint="767px" />
            <ContextMenu model={items_merge} ref={contextRefMerge} breakpoint="767px" />
            <span ref={editarBancoRef} data-bs-toggle="modal" data-bs-target="#modalEditarBanco" />
            {conexoes[index]?.tipo === 0 &&(
            <ModalEditarBanco 
                csrfToken={props.csrfToken} 
                client={props.client} 
                conexao={conexoes[index]}
                projeto_id={props.projeto_id}
                setConexoes={atualizarConexoes}
                setFonte={props.setFonte}
            />
            )}
            <span ref={editarRef} data-bs-toggle="modal" data-bs-target="#modalEditarFonte" />
            <ModalEditarFonte 
                csrfToken={props.csrfToken} 
                client={props.client} 
                conexao={conexoes[index]}
                setSelecionando={setSelecionando}
                setCarregando={props.setCarregando}
                setConexoes={setConexoes}
                setFonte={props.setFonte}
            />
            <span ref={deletarRef} data-bs-toggle="modal" data-bs-target="#modalDeletarFonte" />
            <ModalDeletarFonte 
                csrfToken={props.csrfToken} 
                client={props.client} 
                conexao={conexoes[index]}
                projeto_id={props.projeto_id} 
                setConexoes={atualizarConexoes}
                setFonte={props.setFonte}
            />
            {selecionando &&(
            <ModalBanco 
                csrfToken={props.csrfToken} 
                client={props.client}
                editando={true}
                projeto_id={props.projeto_id} 
                setConexoes={props.setConexoes}
            />
            )}
        </div>  
    );
} 

function ApresentaFonte(props){
    const [dataFrame, setDataFrame] = useState([]);
    const [selecionou, setSelecionou] = useState(false);
    const [estrutura, setEstrutura] = useState([]);
    const [reload, setReload] = useState(true);
    const csrfToken = props.csrfToken;
    const tipo = props.fonte.tipo;
    const tecnologia_local = props.fonte.tecnologia;
    const banco = props.fonte.banco;
    const fonte_id = props.fonte.id;
    const config = {
        headers: {
            'X-CSRFToken': csrfToken,
        }
    };

    useEffect(() => {
        if (fonte_id){
            const formData = new FormData();
            formData.append('fonte_id', fonte_id);
            
            props.client.post(
                "/pincel/extracao",
                formData,
                config
            ).then(function(res) {
                if (tipo === 0){
                    let resposta = res.data.selecionou;
                    setSelecionou(resposta);

                    if (!resposta){
                        props.client.post(
                            "/papiro/seleciona",
                            formData,
                            config
                        ).then(function(res) {
                            setEstrutura(res.data.data);
                        });
                    }else{
                        setDataFrame(res.data.data);
                        setReload(true);
                    }
                }else{
                    setDataFrame(res.data.data);
                    setReload(true);
                }                      
            });
        }        
    }, [props.fonte]);


    return(
        <div className="detalhes">
            <p className="detalhes_titulo">Detalhes</p>
            {tipo === 0 ?(
            <div className="row mb-3">
                <div className="row col">
                    <div className="col-md-4"><label>Banco: </label></div>
                    <div className="col-md-8"><input type="text" className="form-control" value={banco} disabled/></div>
                </div>
                <div className="row col">
                    <div className="col-md-4"><label>SGBD: </label></div>
                    <div className="col-md-8"><input type="text" className="form-control" value={tecnologia[tipo][tecnologia_local]?.nome} disabled/></div>
                </div>
            </div>
            ):(
            <div className="row mb-3">
                <div className="row col">
                    <div className="col-md-4"><label>Arquivo: </label></div>
                    <div className="col-md-8"><input type="text"  className="form-control" value={banco} disabled/></div>
                </div>
                <div className="row col">
                    <div className="col-md-4"><label>Extensão: </label></div>
                    <div className="col-md-8"><input type="text" className="form-control" value={tecnologia[tipo][tecnologia_local]?.nome} disabled/></div>
                </div>
            </div>
            )}
            {(tipo === 0 && selecionou) || tipo !== 0?(
                dataFrame.length > 1 &&(
                    <Tabela 
                        csrfToken={csrfToken}
                        client={props.client}
                        fonte_id={fonte_id}
                        dataFrame={dataFrame}
                        reload={reload}
                        setDataFrame={setDataFrame}
                        setReload={setReload}
                    />
                )                
            ):(
                <SelecionaItens csrfToken={csrfToken} client={props.client} fonte_id={fonte_id} estrutura={estrutura} setFonte={props.setFonte} />                
            )}
        </div>    
    );
}

function SelecionaItens(props){
    const [matriz, setMatriz] = useState([]);
    const [selecionados, setSelecionados] = useState({});
    const csrfToken = props.csrfToken;
    const config = {
        headers: {
            'X-CSRFToken': csrfToken,
        }
    };

    useEffect(() => {
        setMatriz(props.estrutura);
        setSelecionados({});
    }, [props.estrutura]);
    
    const handleSelecionar = (categoria, atributo) => {
        const novosSelecionados = { ...selecionados };

        if (!novosSelecionados[categoria]) {
            novosSelecionados[categoria] = [];
        }
      
        const indice = novosSelecionados[categoria].indexOf(atributo);
      
        if (indice === -1) {
            novosSelecionados[categoria].push(atributo);
        } else {
            novosSelecionados[categoria].splice(indice, 1);
            
            if(novosSelecionados[categoria].length === 0){
                delete novosSelecionados[categoria];
            }
        }
      
        setSelecionados(novosSelecionados);
    };

    const enviarSelecionados = () => {
        const formData = new FormData();
        const fonte_id = props.fonte_id;
        let relacao = [];

        formData.append("fonte_id", fonte_id);
        formData.append("selecionados", JSON.stringify(selecionados));
        
        for(let item in selecionados){
            let categoria1 = document.getElementById(item+'1');
      
            if(categoria1){
              let categoria2 = document.getElementById(item+'2');
              let var1 = document.getElementById("var_"+item+'1');
              let var2 = document.getElementById("var_"+item+'2');
      
              relacao.push([categoria1.value,var1.value,categoria2.value,var2.value]);
            }      
        }
          
        if(relacao.length!== 0){
            formData.append('relacao',JSON.stringify(relacao ));
        }
        
        props.client.post(
            "/pincel/alteracao",
            formData,
            config
        ).then(function(res) {
            props.setFonte({id: 0, tipo: 0, tecnologia: 0, banco: "", data_criacao: ""});
        }).catch(error => {
            if (error.response) {
            console.log('Erro de resposta do servidor:', error.response.data);
            } else if (error.request) {
            console.log('Sem resposta do servidor:', error.request);
            } else {
            console.log('Erro ao processar a solicitação:', error.message);
            }
        });
    };
    
    return (
        <div>
            <div className="card-body">
              <nav className="d-flex justify-content-center">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">Fonte</li>
                  <li className="breadcrumb-item">Selecionar Dados</li>
                </ol>
              </nav>
            </div>
            <div className="row">
                {matriz.map(([categoria, atributos], index) => (
                <div key={index} className ="col-sm-4 detalhes_card">
                    <div className="detalhes_categoria">
                        <p className="detalhes_colunas">{categoria}</p>
                        {atributos.map((atributo, atIndex) => (
                        <div key={atIndex} className="form-check">
                            <input className="form-check-input" type="checkbox" checked={selecionados[categoria] && selecionados[categoria].includes(atributo)} onChange={() => handleSelecionar(categoria, atributo)} id={`${categoria}-${atributo}`} />
                            <label className="form-check-label" htmlFor={`${categoria}-${atributo}`}>
                                {atributo}
                            </label>
                        </div>
                        ))}
                    </div>
                </div>
                ))}
                {Object.keys(selecionados).length > 1 ? (
                    <CarregaCategorias selecionados={selecionados} />
                ): (
                <div id="relacao"></div>
                )}      
                <div className="row">
                    <div className="col-sm-4">
                        <button className="btn btn-primary btn-seleciona" onClick={enviarSelecionados}>Enviar Selecionados</button>
                    </div>    
                </div>
            </div>
        </div>
    );
}

function CarregaCategorias(props){
    let aux = Object.keys(props.selecionados);
  
    const carregaVariaveis = (event) => {
        const id = event.target.id;
        const elemento = document.getElementById(id);
        const variavel = document.getElementById("variavel_"+id)
        let elemento2;
        
        if(id[id.length - 1] ===  '1'){
            elemento2 = document.getElementById(id.slice(0, -1)+"2");
        }else{
            elemento2 = document.getElementById(id.slice(0, -1)+"1");
        }

        if(elemento.value !== ''){
            const opcao = elemento2.querySelector(`option[value="${elemento.value}"]`);
  
            if (opcao) {
                opcao.remove();
            }
            
            for (let chave in props.selecionados) {
                if (chave === elemento.value){
                    let variaveis = props.selecionados[chave];
                    let html = '<label class="col-sm-2 col-form-label">Variavel: </label>'+
                                   '<div class="col-sm-10">'+
                                        '<select id="var_'+id+'" class="form-select" aria-label="Default select example" required>'+
                                            '<option value="">Selecione uma variavel.</option>'
  
                    for(let item in variaveis){
                        html+= '<option value="'+variaveis[item]+'">'+variaveis[item]+'</option>'
                    }
            
                    html += '</select></div>'
  
                    variavel.innerHTML = html;
                }
            }
        }else{
            const opcoes_elemento1 = elemento.options;
            const opcoes_elemento2 = elemento2.options;
            
            for (let i = 0; i < opcoes_elemento1.length; i++) {
                const option = opcoes_elemento1[i];

                if(option.value !== ""){
                    option.remove();
                    i = 0; 
                }
            }

            function valorEstaPresente(valor) {
                for (let i = 0; i < opcoes_elemento2.length; i++) {
                    if (opcoes_elemento2[i].value === valor) {
                        return true;
                    }
                }
                return false;
            }
            
            for (let i=0; i < aux.length; i++){
                if (elemento2.value !== aux[i]){
                    const optionElement = document.createElement("option");

                    optionElement.value = aux[i];
                    optionElement.textContent = aux[i];
                    elemento.appendChild(optionElement);
                }
                
                if (!valorEstaPresente(aux[i])){
                    const optionElement2 = document.createElement("option");

                    optionElement2.value = aux[i];
                    optionElement2.textContent = aux[i];
                    elemento2.appendChild(optionElement2);
                }
            }
            
            variavel.innerHTML = "";
        }
    };
  
    return(
        <div>
            <div className="card-body">
                <nav className="d-flex justify-content-center">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">Fonte</li>
                        <li className="breadcrumb-item">Relações</li>
                    </ol>
                </nav>
            </div>  
            <div>
            {aux.slice(0, aux.length - 1).map((categoria, index) => (
                <div className='detalhes_relacao' key={index}>
                    <p className="relacao_titulo">Relação {index+1}:</p>
                    <div className="row linha">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-2">
                                    <label className="col-form-label">Conjunto: </label>    
                                </div>
                                <div className="col-md-10">
                                    <select id={`${categoria}1`} className="form-select" aria-label="Default select example" onChange={(event) => carregaVariaveis(event)} required>
                                        <option value="">Selecione qual conjunto quer relacionar</option>
                                        {aux.map((categoria) => (
                                        <option value={categoria} key={categoria}>{categoria}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-2">
                                    <label className="col-form-label">Conjunto: </label>    
                                </div>
                                <div className="col-md-10">
                                    <select id={`${categoria}2`} className="form-select" aria-label="Default select example" onChange={(event) => carregaVariaveis(event)} required>
                                        <option value="">Selecione qual conjunto quer relacionar</option>
                                        {aux.map((categoria) => (
                                        <option value={categoria} key={categoria}>{categoria}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div className="row linha">
                        <div className="col-md-6">
                            <div id={`variavel_${categoria}1`} className="row"></div>
                        </div>
                        <div className="col-md-6">
                            <div id={`variavel_${categoria}2`} className="row"></div>
                        </div>
                    </div>                    
                </div>
            ))}
            </div>
        </div>
    );
}

function Navegacao(props){
    function avancar(){
        props.setEtapa(props.etapa+1);
    }

    function voltar(){
        props.setEtapa(props.etapa-1);
    }


    return (
        <>
        {props.temFonte && (
            <>
              {props.etapa === 0 && (
                <div className="navegacao-container">
                  <div className="row">
                    <div className="col-2 navegacao">
                      <button type="button" onClick={voltar} className="btn btn-navegacao-disabled" disabled><i className="bi bi-caret-left-fill"></i> Retornar</button>
                    </div> 
                    <div className="col-8"></div>
                    <div className="col-2 navegacao">
                      <button type="button" onClick={avancar} className="btn btn-navegacao">Avançar <i className="bi bi-caret-right-fill"></i></button>
                    </div> 
                  </div>
                </div>
              )}
      
              {props.etapa === 1 && (
                <div className="navegacao-container">
                  <div className="row">
                    <div className="col-2 navegacao">
                      <button type="button" onClick={voltar} className="btn btn-navegacao"><i className="bi bi-caret-left-fill"></i> Retornar</button>
                    </div> 
                    <div className="col-8"></div>
                    <div className="col-2 navegacao">
                      <button type="button" onClick={avancar} className="btn btn-navegacao">Avançar <i className="bi bi-caret-right-fill"></i></button>
                    </div> 
                  </div>
                </div>
              )}
              
              {props.etapa === 2 && (
                <div className="navegacao-container">
                  <div className="row">
                    <div className="col-2 navegacao">
                      <button type="button" onClick={voltar} className="btn btn-navegacao"><i className="bi bi-caret-left-fill"></i> Retornar</button>
                    </div> 
                    <div className="col-8"></div>
                    <div className="col-2 navegacao">
                      <button type="button" onClick={avancar} className="btn btn-navegacao-disabled" disabled>Avançar <i className="bi bi-caret-right-fill"></i></button>
                    </div> 
                  </div>
                </div>
              )}
            </>
        )}       
    </>
    );
}