import React from 'react';

const Carregando = () => {
  return (
    <div className="loading-screen-overlay">
      <div className="loading-screen-modal">
        <img src="/assets/img/logo.png" alt="" />
        <br />
        <div className="spinner-border loading-thoth" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    </div>
  );
};

export default Carregando;