import React, { useState, useRef, useEffect } from 'react';
import { tecnologia } from './Valores';

export default function ModalConectar(props){
  const csrfToken = props.csrfToken;
  const buttonRef = useRef(null);
  const [formSGDB, setFormSGDB] = useState({tecnologia: '', servidor: '', usuario: '', senha: '', porta: ''});
  const [formArquivo, setFormArquivo] = useState({tecnologia: '', file: null});
  const config = {
    headers: {
        'X-CSRFToken': csrfToken,
    }
  };

  const limparCampos = () => {
    setFormSGDB({tecnologia: '', servidor: '', usuario: '', senha: '', porta: '', banco: ''});
    setFormArquivo({tecnologia: '', file: null});
    document.getElementById('formFile').value = null;
  };
  
  const mudancaSGBD = (e) => {
    setFormSGDB({ ...formSGDB, [e.target.name]: e.target.value });
  }

  const arquivoInput = (e) => {
    setFormArquivo({ ...formArquivo, [e.target.name]: e.target.value });
  }

  const mudancaArquivo = (e) => {
    setFormArquivo({ ...formArquivo, file: e.target.files[0] });
  }

  const enviarFormSGBD = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('tipo', 0)
    formData.append('tecnologia', formSGDB.tecnologia);
    formData.append('servidor', formSGDB.servidor);
    formData.append('usuario', formSGDB.usuario);
    formData.append('senha', formSGDB.senha);
    formData.append('porta', formSGDB.porta);
    console.log(formSGDB);
    props.client.post(
      "/papiro/esquema",
      formData,
      config
    ).then(response => {
      const dados = response.data.conexao;
      const bancos = response.data.bancos
      const conexao = {
        tipo: dados.tipo, 
        tecnologia: dados.tecnologia, 
        servidor: dados.servidor, 
        usuario: dados.usuario,
        senha: dados.senha,
        porta: dados.porta
      }
        
      props.setSelecionando('Banco');
      sessionStorage.setItem('conexao', JSON.stringify(conexao));
      sessionStorage.setItem('bancos', JSON.stringify(bancos));
        
      limparCampos();
        
      if (buttonRef.current) {
        buttonRef.current.click();
      }
    }).catch(err => console.log(err));
  }

  const enviarFormArquivo = (e) => {
    e.preventDefault();
    const nome = formArquivo.file.name
    const extensao = nome.split(".").pop().toLowerCase();
    const tecnologia_elemento = document.getElementById('tecnologia_arquivo');
    const formFile = document.getElementById('formFile');
    const erro = document.getElementById('erro_arquivo');
    
    if(extensao === "xls" || extensao === "xlsx" || extensao === "csv"){
      erro.innerHTML = "";
      
      if (tecnologia_elemento) {
        tecnologia_elemento.classList.remove('is-invalid');
      }
      if (formFile) {
        formFile.classList.remove('is-invalid');
      }
      
      if(extensao === tecnologia[1][parseInt(formArquivo.tecnologia)]?.nome.toLowerCase() || (extensao === "xlsx" && formArquivo.tecnologia === '1')){
        const formData = new FormData();
        formData.append('tecnologia', formArquivo.tecnologia);
        formData.append('arquivo', formArquivo.file);
        formData.append('projeto_id', props.projeto.id)
        
        props.setCarregando(true);

        props.client.post(
          "/pincel/fonte",
          formData,
          config
        ).then(response => {
          props.setCarregando(false);
          limparCampos();
          props.setConexoes(response.data.data);
          props.setSelecionando('');
          buttonRef.current.click();
        });
      }else{
        tecnologia_elemento.classList.add('is-invalid');
        if(erro){
          erro.innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="bi bi-exclamation-octagon me-1"></i>O campo tipo deve refletir a extensão do arquivo<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
        }
      }
    }else{
      if (tecnologia_elemento) {
        tecnologia_elemento.classList.add('is-invalid');
      }
      if (formFile) {
        formFile.classList.add('is-invalid');
      }
      if(erro){
        erro.innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="bi bi-exclamation-octagon me-1"></i>Arquivo com extensão incorreta!<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
      }
    }
  }

  return(
    <div className="modal fade" id="modalConectar" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Conectar a uma fonte de Dados</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <ul className="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
              <li className="nav-item flex-fill" role="presentation">
                <button className="nav-link w-100 active" id="sgdb-tab" data-bs-toggle="tab" data-bs-target="#sgdb" type="button" role="tab" aria-controls="sgdb" aria-selected="false" tabIndex="-1">SGBD</button>
              </li> 
              <li className="nav-item flex-fill" role="presentation">
                <button className="nav-link w-100" id="arquivo-tab" data-bs-toggle="tab" data-bs-target="#arquivo" type="button" role="tab" aria-controls="arquivo" aria-selected="false" tabIndex="-1">Arquivo</button>
              </li>
            </ul>
            <div className="tab-content pt-2" id="borderedTabJustifiedContent">
              <div className="tab-pane fade show active" id="sgdb" role="tabpanel" aria-labelledby='sgbd-tab'>
                <p>Antes de importar dados de um banco de dados, você deve ter em mãos tdoas as informações necessárias para se conectar ao banco de dados.</p>
                <ol>
                  <li>Certifique-se de ter as credenciais corretas do banco de dados, usuário e senha.</li>
                  <li>Certifique-se de ter as informações de acesso ao banco dados, IP ou Endereço do servidor e porta de acesso.</li>
                  <li>Caso a porta fique em branco será usada a porta padrão.</li>
                </ol>
                <form onSubmit={enviarFormSGBD}>
                  <div className="row mb-3">
                    <label htmlFor="tecnologia_sgbd" className="col-sm-3 col-form-label">SGDB</label>
                    <div className="col-sm-9">
                      <select id="tecnologia_sgbd" name="tecnologia" value={formSGDB.tecnologia} onChange={mudancaSGBD} className="form-select" aria-label="Default select example" required>
                        <option value="">Selecione um SGBD</option>
                        <option value="0">MongoDB</option>
                        <option value="1">MySQL</option>
                        <option value="2">PostgreSQL</option>
                      </select>
                    </div>
                  </div>
                  <div className="row mb-3">
                    <label htmlFor="usuario" className="col-sm-3 col-form-label">Usuário</label>
                    <div className="col-sm-9">
                      <input type="text" id="usuario_sgbd" name="usuario" value={formSGDB.usuario} onChange={mudancaSGBD} className="form-control" required/>
                    </div>
                  </div>
                  <div className="row mb-3">
                    <label htmlFor="senha" className="col-sm-3 col-form-label">Senha</label>
                    <div className="col-sm-9">
                      <input type="password" id="senha_sgbd" name="senha" value={formSGDB.senha} onChange={mudancaSGBD} className="form-control" autoComplete="off" required/>
                    </div>
                  </div>
                  <div className="row mb-3">
                    <label htmlFor="servidor" className="col-sm-3 col-form-label">Servidor</label>
                    <div className="col-sm-9">
                      <input type="text" id="servidor_sgbd" name="servidor" value={formSGDB.servidor} onChange={mudancaSGBD} className="form-control" required/>
                    </div>
                  </div>
                  <div className="row mb-3">
                    <label htmlFor="porta" className="col-sm-3 col-form-label">Porta</label>
                    <div className="col-sm-9">
                      <input type="number" id="porta_sgbd" name="porta" value={formSGDB.porta} onChange={mudancaSGBD} className="form-control" min="0"/>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-primary">Conectar</button>
                </form>
              </div>
              <div className="tab-pane fade show " id="arquivo" role="tabpanel" aria-labelledby='arquivo-tab'>
                <p>O sistema atualmente suporta arquivos xls, xlsx e csv.</p>
                <div id="erro_arquivo"></div>
                <form onSubmit={enviarFormArquivo}>
                <div className="row mb-3">
                    <label htmlFor="tecnologia_arquivo" className="col-sm-3 col-form-label">Tipo</label>
                    <div className="col-sm-9">
                      <select id="tecnologia_arquivo" name="tecnologia" value={formArquivo.tecnologia} onChange={arquivoInput} className="form-select" aria-label="Default select example" required>
                        <option value="">Tipo de arquivo</option>
                        <option value="1">Excel xls ou xlsx</option>
                        <option value="0">CSV</option>
                      </select>
                    </div>
                  </div>
                  <div className="row mb-3">
                    <label htmlFor="arquivo" className="col-sm-3 col-form-label">Arquivo</label>
                    <div className="col-sm-9">
                      <input className="form-control" type="file" id="formFile" onChange={mudancaArquivo} required/>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-primary">Carregar</button>
                </form>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" ref={buttonRef} className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
  );  
}

export function ModalBanco(props){
  const csrfToken = props.csrfToken;
  const abrirRef = useRef(null);
  const fecharRef = useRef(null);
  const [conexao, setConexao] = useState({id: '', tipo: '', tecnologia: '', servidor: '', usuario: '', senha: '', porta: '', banco: ''});
  const [bancos, setBancos] = useState([]);
  const [banco, setBanco] = useState('');
  const [editando, setEditando] = useState(false);
  const config = {
    headers: {
        'X-CSRFToken': csrfToken,
    }
  };
  
  useEffect(() => {
    abrirRef.current.click();
    let aux = JSON.parse(sessionStorage.getItem('conexao'));
    setBancos(JSON.parse(sessionStorage.getItem('bancos')));
    setConexao({
      id: aux.id,
      tipo: aux.tipo,
      tecnologia: aux.tecnologia,
      servidor: aux.servidor,
      usuario: aux.usuario,
      senha: aux.senha,
      porta: aux.porta,
      banco: ''
    });
    
    if (props.editando){
      setEditando(true);
    }
  }, []); 

  function limparCampos(){
    setConexao({tipo: '', tecnologia: '', servidor: '', usuario: '', senha: '', porta: '', banco: ''})
    setBancos([]);
    setBanco('');
  }

  const conectarBanco = (e) => {
    e.preventDefault();
    const formData = new FormData();
    let projeto = JSON.parse(sessionStorage.getItem('projeto'));
    formData.append('id', conexao.id)
    formData.append('tipo', 0)
    formData.append('tecnologia', conexao.tecnologia);
    formData.append('servidor', conexao.servidor);
    formData.append('usuario', conexao.usuario);
    formData.append('senha', conexao.senha);
    formData.append('porta', conexao.porta);
    formData.append('banco', banco);
    formData.append('projeto_id', projeto.id);

    if(conexao.id){
      props.client.put(
        "/pincel/fonte",
        formData,
        config
      ).then(function(res) {
        props.setConexoes(res.data.data);
        limparCampos();
        fecharRef.current.click();                         
      });
    }else{
      props.client.post(
        "/pincel/fonte",
        formData,
        config
      ).then(response => {
        limparCampos();
        props.setConexoes(response.data.data);
        props.setSelecionando('');
        fecharRef.current.click();
      })
    }
  }

  return(
    <div>
      <span ref={abrirRef} data-bs-toggle="modal" data-bs-target="#modalBanco" />
      <div className="modal fade" id="modalBanco" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Selecione a Base de Dados</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <form onSubmit={conectarBanco}>
                <div className="row mb-3">
                  <label htmlFor="tecnologia" className="col-sm-3 col-form-label">Tipo</label>
                  <div className="col-sm-9">
                    <input type="text" id="tecnologia_banco" name="tecnologia" value={tecnologia[0][conexao.tecnologia]?.nome || ''} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="usuario" className="col-sm-3 col-form-label">Usuário</label>
                  <div className="col-sm-9">
                    <input type="text" id="usuario_banco" name="usuario" value={conexao.usuario} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="senha" className="col-sm-3 col-form-label">Senha</label>
                  <div className="col-sm-9">
                    <input type="password" id="senha_banco" name="senha" value={"123456789"} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="servidor" className="col-sm-3 col-form-label">Servidor</label>
                  <div className="col-sm-9">
                    <input type="text" id="servidor_banco" name="servidor" value={conexao.servidor} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="porta" className="col-sm-3 col-form-label">Porta</label>
                  <div className="col-sm-9">
                    <input type="text" id="porta_banco" name="porta" value={conexao.porta} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="banco" className="col-sm-3 col-form-label">Banco</label>
                  <div className="col-sm-9">
                    <select onChange={e => setBanco(e.target.value)} className='form-select' required> 
                      <option value="">Selecione um Banco.</option>
                      {bancos.map((item, index) => (
                      <option key={index} value={item}>{item}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <button type="submit" className="btn btn-primary">Conectar</button>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" ref={fecharRef} className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div>    
  );
}

export function ModalEditarBanco(props){
  const csrfToken = props.csrfToken;
  const fecharRef = useRef(null);
  const [conexao, setConexao] = useState({tipo: 0, tecnologia: 0, servidor: '', usuario: '', senha: '', porta: '', banco: ""});
  const [bancos, setBancos] = useState([]);
  const [banco, setBanco] = useState('');
  const config = {
    headers: {
        'X-CSRFToken': csrfToken,
    }
  };

  useEffect(() => {
    if(props.conexao){
      props.client.get(
        "/pincel/fonte/"+props.conexao.id,
        config
      ).then(response => {
        let retorno = response.data.data;
        setConexao({
          tipo: parseInt(retorno.tipo,10),
          tecnologia: parseInt(retorno.tecnologia,10),
          servidor: retorno.servidor, 
          usuario: retorno.usuario, 
          senha: retorno.senha, 
          porta: retorno.porta,
          banco: retorno.banco
        })
        setBanco(retorno.banco);

        const formData = new FormData();
        formData.append('tipo', retorno.tipo);
        formData.append('tecnologia', retorno.tecnologia);
        formData.append('servidor', retorno.servidor);
        formData.append('porta', retorno.porta);
        formData.append('usuario', retorno.usuario);
        formData.append('senha', retorno.senha);
        
        props.client.post(
          "/papiro/esquema",
          formData,
          config
        ).then(response => {
          setBancos(response.data.bancos);
        });
      });
    }
  }, [props.conexao]);

  function salvarBanco(e){
    e.preventDefault();

    const formData = new FormData();
    formData.append('banco', banco);
    
    props.client.post(
      "/pincel/fonte/"+props.conexao.id+"/",
      formData,
      config
    ).then(response => {
      const formData = new FormData();
      formData.append('projeto_id', props.projeto_id);
        
      props.client.post(
          "/pincel/fonte",
          formData,
          config
      ).then(function(res) {
        props.setConexoes(res.data.data)                         
      });
      fecharRef.current.click();
    });
  }

  
  return(
    <div>
      <div className="modal fade" id="modalEditarBanco" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Editar Banco de Dados</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
            <form onSubmit={salvarBanco}>
                <div className="row mb-3">
                  <label htmlFor="tecnologia" className="col-sm-3 col-form-label">Tipo</label>
                  <div className="col-sm-9">
                    <input type="text" id="tecnologia" name="tecnologia" value={tecnologia[conexao.tipo][conexao.tecnologia]?.nome || ''} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="usuario" className="col-sm-3 col-form-label">Usuário</label>
                  <div className="col-sm-9">
                    <input type="text" id="usuario" name="usuario" value={conexao.usuario} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="senha" className="col-sm-3 col-form-label">Senha</label>
                  <div className="col-sm-9">
                    <input type="password" id="senha" name="senha" value={"123456789"} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="servidor" className="col-sm-3 col-form-label">Servidor</label>
                  <div className="col-sm-9">
                    <input type="text" id="servidor" name="servidor" value={conexao.servidor} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="porta" className="col-sm-3 col-form-label">Porta</label>
                  <div className="col-sm-9">
                    <input type="text" id="porta" name="porta" value={conexao.porta} className="form-control" readOnly disabled/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="banco" className="col-sm-3 col-form-label">Banco</label>
                  <div className="col-sm-9">
                    <select onChange={e => setBanco(e.target.value)} className='form-select' value={banco} required> 
                      <option value="">Selecione um Banco.</option>
                      {bancos.map((item, index) => (
                      <option key={index} value={item}>{item}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <button type="submit" className="btn btn-primary">Conectar</button>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" ref={fecharRef} className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div>    
  );
}

export function ModalEditarFonte(props){
  const csrfToken = props.csrfToken;
  const fecharRef = useRef(null);
  const [formSGDB, setFormSGDB] = useState({tecnologia: '', servidor: '', usuario: '', senha: '', porta: ''});
  const [formArquivo, setFormArquivo] = useState({tecnologia: '', file: null});
  const config = {
    headers: {
        'X-CSRFToken': csrfToken,
    }
  };

  useEffect(() => {
    if (props.conexao){
      if(props.conexao.tipo === 0){
        props.client.get(
          "/pincel/fonte/"+props.conexao.id,
          config
        ).then(response => {
          let retorno = response.data.data;
          setFormSGDB({
            tecnologia: retorno.tecnologia,
            servidor: retorno.servidor, 
            usuario: retorno.usuario, 
            senha: '123456', 
            porta: retorno.porta
          })
        });
      }else{
        setFormArquivo({tecnologia: props.conexao.tecnologia, file: null});
      }
    }
  }, [props.conexao]); 
  
  const mudancaSGBD = (e) => {
    setFormSGDB({ ...formSGDB, [e.target.name]: e.target.value });
  }

  const arquivoInput = (e) => {
    setFormArquivo({ ...formArquivo, [e.target.name]: e.target.value });
  }

  const mudancaArquivo = (e) => {
    setFormArquivo({ ...formArquivo, file: e.target.files[0] });
  }

  function editarBanco(){
    const formData = new FormData();
    formData.append('tipo', 0)
    formData.append('tecnologia', formSGDB.tecnologia);
    formData.append('servidor', formSGDB.servidor);
    formData.append('usuario', formSGDB.usuario);
    formData.append('senha', formSGDB.senha);
    formData.append('porta', formSGDB.porta);
    
    props.client.post(
      "/papiro/esquema",
      formData,
      config
    ).then(response => {
      const dados = response.data.conexao;
      const bancos = response.data.bancos
      const conexao = {
        id: props.conexao.id,
        tipo: dados.tipo, 
        tecnologia: dados.tecnologia, 
        servidor: dados.servidor, 
        usuario: dados.usuario,
        senha: dados.senha,
        porta: dados.porta
      }
        
      props.setSelecionando('Banco');
      sessionStorage.setItem('conexao', JSON.stringify(conexao));
      sessionStorage.setItem('bancos', JSON.stringify(bancos));
      
      props.setSelecionando(true);
      fecharRef.current.click();  
    }).catch(err => console.log(err));
  }

  const limparCampos = () => {
    setFormSGDB({tecnologia: '', servidor: '', usuario: '', senha: '', porta: ''});
    setFormArquivo({tecnologia: '', file: null});
    document.getElementById('formFile_editar').value = null;
  };

  function editarArquivo(e){
    e.preventDefault();
    const nome = formArquivo.file.name
    const extensao = nome.split(".").pop().toLowerCase();
    const tecnologia_elemento = document.getElementById('tecnologia_editar_arquivo');
    const formFile = document.getElementById('formFile_editara');
    const erro = document.getElementById('erro_arquivo');
    
    if(extensao === "xls" || extensao === "xlsx" || extensao === "csv"){
      erro.innerHTML = "";
      
      if (tecnologia) {
        tecnologia_elemento.classList.remove('is-invalid');
      }
      if (formFile) {
        formFile.classList.remove('is-invalid');
      }
      
      if(extensao === tecnologia[1][parseInt(formArquivo.tecnologia)]?.nome.toLowerCase() || extensao === "xlsx"){
        const formData = new FormData();
        formData.append('tipo', 1);
        formData.append('tecnologia', formArquivo.tecnologia);
        formData.append('arquivo', formArquivo.file);
        formData.append('fonte_id', props.conexao.id)
        
        props.setCarregando(true);
        props.client.post(
          "/pincel/fonte/"+props.conexao.id+"/",
          formData,
          config
        ).then(response => {
          props.setCarregando(false);
          limparCampos();
          props.setConexoes(response.data.data);
          props.setSelecionando('');
          fecharRef.current.click();
        });
      }else{
        tecnologia_elemento.classList.add('is-invalid');
        if(erro){
          erro.innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="bi bi-exclamation-octagon me-1"></i>O campo tipo deve refletir a extensão do arquivo<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
        }
      }
    }else{
      if (tecnologia_elemento) {
        tecnologia_elemento.classList.add('is-invalid');
      }
      if (formFile) {
        formFile.classList.add('is-invalid');
      }
      if(erro){
        erro.innerHTML = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="bi bi-exclamation-octagon me-1"></i>Arquivo com extensão incorreta!<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
      }
    }
  }

  return(
    <div>
      <div className="modal fade" id="modalEditarFonte" aria-hidden="true">
        <div className="modal-dialog">
        {props.conexao && props.conexao.tipo === 0 ?(
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Editar Base de Dados</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <form>
                <div className="row mb-3">
                  <label htmlFor="tecnologia_editar" className="col-sm-3 col-form-label">SGDB</label>
                  <div className="col-sm-9">
                    <select id="tecnologia_editar" name="tecnologia" value={formSGDB.tecnologia} onChange={mudancaSGBD} className="form-select" aria-label="Default select example" required>
                      <option value="">Selecione um SGBD</option>
                      <option value="0">MongoDB</option>
                      <option value="1">MySQL</option>
                      <option value="2">PostgreSQL</option>
                    </select>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="usuario_editar" className="col-sm-3 col-form-label">Usuário</label>
                  <div className="col-sm-9">
                    <input type="text" id="usuario_editar" name="usuario" value={formSGDB.usuario} onChange={mudancaSGBD} className="form-control" required/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="senha_editar" className="col-sm-3 col-form-label">Senha</label>
                  <div className="col-sm-9">
                    <input type="password" id="senha_editar" name="senha" value={formSGDB.senha} onChange={mudancaSGBD} className="form-control" autoComplete="off" required/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="servidor_editar" className="col-sm-3 col-form-label">Servidor</label>
                  <div className="col-sm-9">
                    <input type="text" id="servidor_editar" name="servidor" value={formSGDB.servidor} onChange={mudancaSGBD} className="form-control" required/>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="porta_editar" className="col-sm-3 col-form-label">Porta</label>
                  <div className="col-sm-9">
                    <input type="number" id="porta_editar" name="porta" value={formSGDB.porta} onChange={mudancaSGBD} className="form-control" min="0"/>
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" onClick={() => editarBanco()}>Salvar</button>
              <button type="button" ref={fecharRef} className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
          </div>
        ):(
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Editar Base de Dados</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <p>O sistema atualmente suporta arquivos xls, xlsx e csv.</p>
              <div id="erro_arquivo"></div>
              <form>
                <div className="row mb-3">
                  <label htmlFor="tecnologia_editar_arquivo" className="col-sm-3 col-form-label">Tipo</label>
                  <div className="col-sm-9">
                    <select id="tecnologia_editar_arquivo" name="tecnologia" value={formArquivo.tecnologia} onChange={arquivoInput} className="form-select" aria-label="Default select example" required>
                      <option value="">Tipo de arquivo</option>
                      <option value="1">Excel xls ou xlsx</option>
                      <option value="0">CSV</option>
                    </select>
                  </div>
                </div>
                <div className="row mb-3">
                  <label htmlFor="formFile_editar" className="col-sm-3 col-form-label">Arquivo</label>
                  <div className="col-sm-9">
                    <input className="form-control" type="file" id="formFile_editar" onChange={mudancaArquivo} required/>
                  </div>
                </div>
                
              </form>
            </div>
            <div className="modal-footer">
              <button type="submit" onClick={editarArquivo}className="btn btn-primary">Carregar</button>
              <button type="button" ref={fecharRef} className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
          </div>
        )}          
        </div>
      </div>
    </div>
  );
}

export function ModalDeletarFonte(props){
  const csrfToken = props.csrfToken;
  const fecharRef = useRef(null);
  const config = {
    headers: {
        'X-CSRFToken': csrfToken,
    }
  };
  
  function deletarFonte(){
    props.client.delete(
      "/pincel/fonte/"+props.conexao.id,
      config
    ).then(response => {
      const formData = new FormData();
      formData.append('projeto_id', props.projeto_id);
        
      props.client.post(
          "/pincel/fonte",
          formData,
          config
      ).then(function(res) {
        props.setConexoes(res.data.data);
        props.setFonte({id: 0, tipo: 0, tecnologia: 0, banco: "", data_criacao: ""});                       
      });
      fecharRef.current.click();
    });
  }

  return(
    <div>
      <div className="modal fade" id="modalDeletarFonte" aria-hidden="true">
        {props.conexao &&(
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Deletar Base de Dados</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <p>Realmente deseja remover {props.conexao.tipo === 0 ?("Banco de dados"):("os dados do arquivo")} "{props.conexao.banco}" - {tecnologia[props.conexao.tipo][props.conexao.tecnologia]?.nome}?</p>
              <ol>
                  <li>Todas as alterações feitas para este fonte de dados serão perdidas.</li>
                  <li>Todos os dados para esta fonte serão perdidos.</li>
                  <li>Esta ação não pode ser desfeita, mas a fonte de dados pode ser inserida novamente.</li>
                </ol>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={() => deletarFonte()}>Deletar</button>
              <button type="button" ref={fecharRef} className="btn btn-primary" data-bs-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
        )}
      </div>
    </div>
  );
}

export function ModalNormalizarNota(props){
  const [notaDF, setNotaDF] = useState([]);
  const [notaAuxiliar, setNotaAuxiliar] = useState([]);
  const botaoAbrir = useRef(null);
  const botaoFechar = useRef(null);

  useEffect(() => {
    if (props.normalizar === 0 && props.dataFrame.length === 0 ) {
      const elemento = document.getElementById('erro');
      const alert = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Que Pena! Nenhuma fonte de dados foi selecionada.<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
      elemento.innerHTML = '';
      elemento.innerHTML = alert;
      props.setNormalizar(99); //Atualiza componentes
    } else if (props.normalizar === 1 && props.dataFrame.length > 0 && props.dfAuxiliar.length === 0) {
      const formData = new FormData();
      formData.append('dataFrame', JSON.stringify(props.dataFrame));
  
      props.client.post(
        "/normalizacao/identificaNota",
        formData,
        { headers: { "Content-Type": "multipart/form-data" }})
        .then(response => {
          let resposta = response.data.data
          setNotaDF(resposta[0]);
          
          if (botaoAbrir.current) {
            botaoAbrir.current.click();
          }
        });
        props.setNormalizar(99); //Atualiza componentes
    }else if (props.normalizar === 1 && props.dataFrame.length > 0 && props.dfAuxiliar.length > 0) {
      const formData = new FormData();
      formData.append('dataFrame', JSON.stringify(props.dataFrame));
      formData.append('dfAuxiliar', JSON.stringify(props.dfAuxiliar));
      
      props.client.post(
        "/normalizacao/identificaNota",
        formData,
        { headers: { "Content-Type": "multipart/form-data" }})
        .then(response => {
          let resposta = response.data.data
          
          if(resposta.length === 2){
            setNotaDF(resposta[0]);
            setNotaAuxiliar(resposta[1]);
          }
          
          if (botaoAbrir.current) {
            botaoAbrir.current.click();
          }
        });
        props.setNormalizar(99); //Atualiza componentes
    }
  });
  
  const enviarNormalizarNota = (e) => {
    e.preventDefault();
    const formData = new FormData();
    const metodos = [];
    const decimais = [];
    const metAuxiliar = [];
    const decAuxiliar = [];
    formData.append('dataFrame', JSON.stringify(props.dataFrame));
    formData.append('notas', JSON.stringify(notaDF));
    
    if(props.dfAuxiliar.length > 0){
      formData.append('dfAuxiliar', JSON.stringify(props.dfAuxiliar));
      formData.append('notasAuxiliar', JSON.stringify(notaAuxiliar));
    }

    for(let nota in notaDF){
      let elemento = document.getElementById(`dataFrame_${notaDF[nota]}`);
      let valor = elemento.value;
      
      if(valor === '2'){
        let decimal = document.getElementById(`dataFrame_${notaDF[nota]}_decimal`);
        decimais.push([notaDF[nota], decimal.value]);
      }

      metodos.push(elemento.value);
    } 
    
    if(notaAuxiliar.length > 0){
      for(let nota in notaAuxiliar){
        let elemento = document.getElementById(`dfAuxiliar_${notaAuxiliar[nota]}`);
        let valor = elemento.value;
        
        if(valor === '2'){
          let decimal = document.getElementById(`dfAuxiliar_${notaAuxiliar[nota]}_decimal`);
          decAuxiliar.push([notaAuxiliar[nota], decimal.value]);
        }
  
        metAuxiliar.push(elemento.value);
      }  
      
      formData.append('decAuxiliar', JSON.stringify(decAuxiliar));
      formData.append('metAuxiliar', JSON.stringify(metAuxiliar));    
    }

    formData.append('metodos', JSON.stringify(metodos));
    formData.append('decimais', JSON.stringify(decimais));
    
    props.client.post(
      "/normalizacao/normalizaNota",
      formData,
      { headers: { "Content-Type": "multipart/form-data" }})
      .then(response => {
        let resposta = response.data.data;
        
        if(resposta.length === 0){
          console.log("Nenhuma alteração feita.")
        }else if(resposta[0].length === 0 && resposta[1].length === 0){
          console.log("Nenhuma alteração feita.") 
        }else{
          if(resposta[0].length > 0){
            props.setDataFrame(resposta[0]);
            setNotaDF([]);
          }
          
          if(resposta[1].length > 0){
            props.setDfAuxiliar(resposta[1]);
            setNotaAuxiliar([]);
          }
        } 
        
        props.setNormalizar(99);
        
        if (botaoFechar.current) {
          botaoFechar.current.click();
        }
      }
    );    
  }

  const mostraSelect = (e) => {
    const value = e.target.value;
    const id = e.target.id;
    const elemento = document.getElementById(id+'_div');

    if(value === '2'){
      elemento.innerHTML = `
        <div class="row mb-3 col-sm-12">
          <div class="row mb-3 col-sm-6">
            <div class="row mb-3">
              <label htmlFor="${id}_decimal" class="col-sm-3 col-form-label">Decimal</label>
              <div class="col-sm-9">
                <select id="${id}_decimal" name="${id}_decimal" class="form-select" aria-label="multiple select example">
                  <option value="0">Números Inteiros</option>
                  <option value="1">Uma casa decimal</option>
                  <option value="2">Duas casas decimais</option>
                </select>
              </div>
            </div>
          </div>
        </div>`;
    }else elemento.innerHTML = "";
  }

  return (
    <div>
      <button data-bs-toggle="modal" data-bs-target="#modalDataFrame" ref={botaoAbrir} />
      <div className="modal fade modal-lg" id="modalDataFrame" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Normalizar Notas</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" ref={botaoFechar} />
            </div>
            <div className="modal-body">
              <p>A seguir são apresentados os campos elegiveis para normalização de nota, selecione o campo e o tipo de normalização, caso o campo apresentado não seja uma nota ou não queira alterar os seus valores basta nao nao selecionar o tipo.</p>
              
              <form onSubmit={enviarNormalizarNota}>
                <h3>Fonte Principal</h3>
                <hr />
                {notaDF.map((coluna, index) => (
                <div key={index}>
                  <div className="row mb-3 col-sm-12">
                    <div className="row mb-3 col-sm-6">
                      <div className="row mb-3">
                        <label htmlFor={coluna} className="col-sm-3 col-form-label">Coluna</label>
                        <div className="col-sm-9">
                          <input type="text" id={coluna} name={coluna} value={coluna} className="form-control" disabled/>
                        </div>
                      </div>
                    </div>
                    <div className="row mb-3 col-sm-6">
                      <div className="row mb-3">
                        <label htmlFor={`dataFrame_${coluna}`} className="col-sm-3 col-form-label">Tipo</label>
                        <div className="col-sm-9">
                          <select id={`dataFrame_${coluna}`} name={`dataFrame_${coluna}`} className="form-select" onChange={mostraSelect} aria-label="multiple select example">
                            <option value="0">Não alterar</option>
                            <option value="1">Normalizar para letras</option>
                            <option value="2">Normalizar para números</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id={`dataFrame_${coluna}_div`}></div>
                </div>
                ))}
                {notaAuxiliar.length > 0 && (
                <div>
                  <h3>Fonte Secundária</h3>
                  <hr />
                </div>
                )}
                {notaAuxiliar.map((coluna, index) => (
                <div key={index}>
                  <div className="row mb-3 col-sm-12">
                    <div className="row mb-3 col-sm-6">
                      <div className="row mb-3">
                        <label htmlFor={coluna} className="col-sm-3 col-form-label">Coluna</label>
                        <div className="col-sm-9">
                          <input type="text" id={coluna} name={coluna} value={coluna} className="form-control" disabled/>
                        </div>
                      </div>
                    </div>
                    <div className="row mb-3 col-sm-6">
                      <div className="row mb-3">
                        <label htmlFor={`dfAuxiliar_${coluna}`} className="col-sm-3 col-form-label">Tipo</label>
                        <div className="col-sm-9">
                          <select id={`dfAuxiliar_${coluna}`} name={`dfAuxiliar_${coluna}`} className="form-select" onChange={mostraSelect} aria-label="multiple select example">
                            <option value="0">Não alterar</option>
                            <option value="1">Normalizar para letras</option>
                            <option value="2">Normalizar para números</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id={`dfAuxiliar_${coluna}_div`}></div>
                </div>
                ))}
                <button type="submit" className="btn btn-primary">Enviar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export function ModalNovoProjeto(props){
  const buttonRef = useRef(null);
  const [formProjeto, setFormProjeto] = useState({nome: '', descricao: ''});

  const mudancaProjeto = (e) => {
    setFormProjeto({ ...formProjeto, [e.target.name]: e.target.value });
  }

  const enviarFormProjeto = (e) => {
    e.preventDefault();
    const csrfToken = props.csrfToken;
    const formData = new FormData();
    formData.append('nome', formProjeto.nome);
    formData.append('descricao', formProjeto.descricao);

    const config = {
        headers: {
            'X-CSRFToken': csrfToken,
        }
    };

    props.client.post(
        "/pincel/projeto",
        formData,
        config
    ).then(function(res) {
        const novo_projeto = {id: res.data.data, nome: formProjeto.nome, descricao: formProjeto.descricao};

        props.setProjetoAtivo(true);
        props.setProjeto(novo_projeto);
        props.setSelecionando("");
        sessionStorage.setItem('projeto', JSON.stringify(novo_projeto));
        setFormProjeto({nome: '', descricao: ''});
        if (buttonRef.current) {
          buttonRef.current.click();
        }
    });
  }
    
  return(
    <div className="modal fade" id="modalProjeto" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Criar um novo projeto</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <form onSubmit={enviarFormProjeto}>
              <div className="row mb-3">
                <label htmlFor="nome" className="col-sm-3 col-form-label">Nome</label>
                <div className="col-sm-9">
                  <input type="text" id="nome" name="nome" value={formProjeto.nome} onChange={mudancaProjeto} className="form-control" required/>
                </div>
              </div>
              <div className="row mb-3">
                <label htmlFor="descricao" className="col-sm-3 col-form-label">Descrição</label>
                <div className="col-sm-9">
                  <textarea type="text" id="descricao" name="descricao" value={formProjeto.descricao} onChange={mudancaProjeto} className="form-control" required/>
                </div>
              </div>
              <button type="submit" className="btn btn-primary col-sm-3">Criar</button>
            </form>
          </div>
          <div className="modal-footer">
            <button type="button" ref={buttonRef} className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export function ModalEditarProjeto(props){
  const csrfToken = props.csrfToken;
  const buttonRef = useRef(null);
  const [formProjeto, setFormProjeto] = useState({nome: props.projeto.nome, descricao: props.projeto.descricao});

  const mudancaProjeto = (e) => {
    setFormProjeto({ ...formProjeto, [e.target.name]: e.target.value });
  }

  const alterarProjeto = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('id', props.projeto.id);
    formData.append('nome', formProjeto.nome);
    formData.append('descricao', formProjeto.descricao);
  
    const config = {
        headers: {
            'X-CSRFToken': csrfToken,
        }
    };
  
    props.client.put(
        "/pincel/projeto",
        formData,
        config
    ).then(function(res) {
        const storedData = sessionStorage.getItem('projeto');

        if (storedData) {
          const projeto_sessao = JSON.parse(storedData);
          const projeto = {id: props.projeto.id, nome: formProjeto.nome, descricao: formProjeto.descricao};

          if (projeto_sessao.id === props.projeto.id){
            props.setProjeto(projeto);
            sessionStorage.setItem('projeto', JSON.stringify(projeto));
          }
        }  

        props.setRecarregar(true);
        
        if (buttonRef.current) {
          buttonRef.current.click();
        } 
    });
  }
    
  return(
    <div className="modal fade" id={"modalEditaProjeto"+props.projeto.id} aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Alterar projeto</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <form onSubmit={alterarProjeto}>
              <div className="row mb-3">
                <label htmlFor="nome" className="col-sm-3 col-form-label">Nome</label>
                <div className="col-sm-9">
                  <input type="text" id="nome" name="nome" value={formProjeto.nome} onChange={mudancaProjeto} className="form-control" required/>
                </div>
              </div>
              <div className="row mb-3">
                <label htmlFor="descricao" className="col-sm-3 col-form-label">Descrição</label>
                <div className="col-sm-9">
                  <textarea type="text" id="descricao" name="descricao" value={formProjeto.descricao} onChange={mudancaProjeto} className="form-control" required/>
                </div>
              </div>
              <button type="submit" className="btn btn-primary col-sm-3">Enviar</button>
            </form>
          </div>
          <div className="modal-footer">
            <button type="button" ref={buttonRef} className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
  );
}