export const tipo = [
    [0, 'Banco'],
    [1, 'Arquivo'],
]

export const tecnologia = { 
    0: {
        0: {
            "nome": "MongoDB",
            "imagem": "/assets/img/fonte/mongodb_logo.png"
        }, 
        1:{
            "nome": "MySQL",
            "imagem": "/assets/img/fonte/mysql_logo.png"
        },
        2:{
            "nome": "PostgreSQL",
            "imagem": "/assets/img/fonte/postgresql_logo.png"
        } 
    },
    1: {
        0:{
            "nome": "CSV",
            "imagem": "/assets/img/fonte/csv.png"
        },
        1:{
            "nome": "XLS",
            "imagem": "/assets/img/fonte/xls.png"
        },
        2:{
            "nome": "XLSX",
            "imagem": "/assets/img/fonte/xlsx.png"
        } 
    }, 
    2: {
        0:{
            "nome": "Merged - Vetical",
            "imagem": "assets/img/fonte/merged.png"
        },
        1:{
            "nome": "Merged - Horizontal",
            "imagem": "assets/img/fonte/merged.png"
        }
    },
}

/*
Tipos de alterações

# Tipo e Valor

Tipo 0 - Seleção
Valor - Dicionario onde as chaves são os nomes das tabelas e os valores são listas dos atributos selecionados

Tipo 1 - Relação
Valor - uma matriz onde cada linha contem uma lista de 4 posições sendo tabela1 atributo1 tabela2 e atributo2, esses atributos devem se relacionar.

Tipo 2 - Editar
Valor - Matriz onde cada linha tem 1 ou 2 posições, se tiver uma é coluna, se tiver 2 celula* 

Tipo 3 - Remover
Valor - Matriz onde cada linha tem 1 ou 2 posições, a primeira indica se é linha ou coluna, a segunda é o index a ser deletado.
[0,0] - A primeira coluna do DataFrame
[1,3] - A quarta linha do DataFrame

Tipo 4 - Junção de Fontes
Valor - uma matriz contendo 3 listas, a primeira lista é o id das duas fontes, a segunda e a terceira linhas correspondem aos nomes das features das duas fontes.  
[[],[],[]]

Tipo 5 - Normalização
Valor - Uma lista contendo o metodo, a quantidade de casa decimais se houver e os items selecionados.
[0,[0,2]]
[0,0,[0,2]] 

*/