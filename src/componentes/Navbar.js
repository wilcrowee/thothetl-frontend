import React, { useState } from 'react';

export default function Navbar(props) {
  const [isOpen, setIsOpen] = useState(false);

  function toggleDropdown() {
    setIsOpen(!isOpen);
  }

  function botaoDinamico() {
    if (props.registrationToggle) {
      document.getElementById("botaoDinamico").innerHTML = "Registrar";
      props.setRegistrationToggle(false);
    } else {
      document.getElementById("botaoDinamico").innerHTML = "Entrar";
      props.setRegistrationToggle(true);
    }
  }

  function enviarLogout(e) {
    e.preventDefault();
    const csrfToken = props.csrfToken;

    const config = {
      headers: {
          'X-CSRFToken': csrfToken,
      }
    };

    props.client.post(
      "/api/logout",
      null,
      config
    ).then(function(res) {
      props.setCurrentUser(false);
      props.limpaVariaveis();
      setIsOpen(false);
      sessionStorage.clear();
    });
  }

  function listaProjetos() {
    props.setSelecionando("Projeto");
    setIsOpen(false);
  }
    
  if(props.currentUser && props.user){
    return (
      <header id="header" className="header fixed-top d-flex align-items-center">
        <div className="d-flex align-items-center justify-content-between">
          <a href="/" className="logo d-flex align-items-center">
            <img src="/assets/img/logo_icon.png" alt="Logotipo" />
            <img src="/assets/img/texto.png" alt="Texto_ThothETL" />
          </a>
        </div>
        <nav className="header-nav ms-auto">
          <ul className="d-flex align-items-center">
            <li className="nav-item dropdown pe-3">
              <button onClick={toggleDropdown} className="nav-link nav-profile d-flex align-items-center pe-0">
                <img src="assets/img/user_default.png" alt="Profile" className="rounded-circle" />
                <span className="d-none d-md-block dropdown-toggle ps-2">{props.user[0]}</span>
              </button>
              {isOpen && (
              <ul className="navbar-dropdown dropdown-menu dropdown-menu-end dropdown-menu-arrow profile show" data-popper-placement="bottom-end">
                <li className="dropdown-header">
                  <h6>{props.user[0]}</h6>
                  <span>{props.user[1]}</span>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <button className="dropdown-item d-flex align-items-center" onClick={listaProjetos}>
                    <i className="bi bi-folder"></i>
                    <span>Projetos</span>
                  </button>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <button className="dropdown-item d-flex align-items-center" onClick={enviarLogout}>
                    <i className="bi bi-box-arrow-right"></i>
                    <span>Sair</span>
                  </button>
                </li>
              </ul>
              )}
            </li>
          </ul>
        </nav>
      </header>
    );
  }
  return(
    <header id="header" className="header fixed-top d-flex align-items-center">
        <div className="d-flex align-items-center justify-content-between">
          <a href="/" className="logo d-flex align-items-center">
            <img src="/assets/img/logo_icon.png" alt="" />
            <img src="/assets/img/texto.png" alt="" />
          </a>
        </div>
        <nav className="header-nav ms-auto">
          <ul className="d-flex align-items-center">
            <li className="">
              <button id="botaoDinamico" className="btn btn-primary" onClick={botaoDinamico}>Registrar</button>
            </li>
          </ul>
        </nav>         
    </header>
  );
}