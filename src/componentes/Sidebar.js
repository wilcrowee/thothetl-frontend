import React from 'react';
import { ModalNormalizarNota } from './Modal';

export default function Sidebar(props){
  const normalizarNotas = () => { 
    props.setComando("Normalizar_Nota");
  };

  function juntar(){
    props.setJoin(true);
    props.setComando("Juntar");
  }

  if(props.currentUser){  
    if(props.projetoAtivo){
      if(props.etapa === 0){
        return(
          <aside id="sidebar" className="sidebar">
            <ul className="sidebar-nav" id="sidebar-nav">
              <li className="nav-item">
                <a className="nav-link " href="index.html" data-bs-toggle="modal" data-bs-target="#modalConectar">
                  <i className="bi bi-plus"></i>
                  <span>Nova Conexão</span>
                </a>
              </li>
            </ul>
          </aside>
        );      
      }else{
        return(
          <aside id="sidebar" className="sidebar">
            <ul className="sidebar-nav" id="sidebar-nav">
              <li className="nav-item">
                <button className="nav-link" onClick={juntar}>
                  <i className="bi bi-plus"></i>
                  <span>Junção de Dados</span>
                </button>
              </li>
              <li className="nav-item">
                <a className="nav-link collapsed" data-bs-target="#limpar-nav" data-bs-toggle="collapse" href="#">
                  <i className="bi bi-eraser"></i><span>Limpar Dados</span><i className="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="limpar-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                    <a href="components-alerts.html">
                      <i className="bi bi-circle"></i><span>Metodo 1</span>
                    </a>
                  </li>
                  <li>
                    <a href="components-accordion.html">
                      <i className="bi bi-circle"></i><span>Metodo 2</span>
                    </a>
                  </li>
                  <li>
                    <a href="components-accordion.html">
                      <i className="bi bi-circle"></i><span>Metodo 3</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link collapsed" data-bs-target="#outlier-nav" data-bs-toggle="collapse" href="#">
                  <i className="bi bi-exclamation-octagon"></i><span>Detectar Outlier</span><i className="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="outlier-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                    <a href="components-alerts.html">
                      <i className="bi bi-circle"></i><span>Metodo 1</span>
                    </a>
                  </li>
                  <li>
                    <a href="components-accordion.html">
                      <i className="bi bi-circle"></i><span>Metodo 2</span>
                    </a>
                  </li>
                  <li>
                    <a href="components-accordion.html">
                      <i className="bi bi-circle"></i><span>Metodo 3</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link collapsed" data-bs-target="#outros-nav" data-bs-toggle="collapse" href="#">
                  <i className="bi bi-menu-button-wide"></i><span>Outras Funções</span><i className="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="outros-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                    <button className='sidebar-button' onClick={normalizarNotas}>
                      <i className="bi bi-circle"></i><span>Normalizar Notas</span>
                    </button>
                  </li>
                  <li>
                    <a href="components-accordion.html">
                      <i className="bi bi-circle"></i><span>Metodo 2</span>
                    </a>
                  </li>
                  <li>
                    <a href="components-accordion.html">
                      <i className="bi bi-circle"></i><span>Metodo 3</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </aside>
        );
      }
    }else{
      return(
        <aside id="sidebar" className="sidebar">
          <ul className="sidebar-nav" id="sidebar-nav">
            <li className="nav-item">
              <a className="nav-link " href="index.html" data-bs-toggle="modal" data-bs-target="#modalProjeto">
                <i className="bi bi-folder-plus"></i>
                <span>Novo Projeto</span>
              </a>
            </li>
          </ul>
        </aside>
      );
    }
  }else{
    return <aside id="sidebar" className="sidebar"></aside>
  }
}