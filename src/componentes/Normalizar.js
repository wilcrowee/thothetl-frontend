import React, {useEffect, useState} from "react";
import { tecnologia } from './Valores';
import AlertaErro from "./Alertas";

export default function NormalizarNota(props){
    const tipo = props.fonte.tipo;
    const tecnologia_local = props.fonte.tecnologia;
    const banco = props.fonte.banco;
    const fonte_id = props.fonte.id;
    const [dataFrame, setDataFrame] = useState([]);
    const [selecionou, setSelecionou] = useState(false);
    const [notas, setNotas] = useState([]);
    const [mostrarSegundoSelect, setMostrarSegundoSelect] = useState(false);
    const [erroIntero, setErroInterno] = useState("");
    const config = {
        headers: {
            'X-CSRFToken': props.csrfToken,
        }
    };
    
    useEffect(() => {
        if (fonte_id){
            let formData = new FormData();
            formData.append('fonte_id', fonte_id);
            
            props.client.post(
                "/pincel/extracao",
                formData,
                config
            ).then(function(res) {
                if (tipo === 0){
                    let resposta = res.data.selecionou;
                    setSelecionou(resposta);

                    if (!resposta){
                        props.setMsg("As features desta fonte de dados ainda não foram selecionadas.");
                        props.setComando("");
                    }else{
                        setDataFrame(res.data.data);
                    }
                }else{
                    setDataFrame(res.data.data);
                }      
                
                if (res.data.data.length > 0){
                    props.client.post(
                        "/normalizar/identifica_nota",
                        formData,
                        config
                    ).then(function(res) {
                        console.log(res.data.data);
                        if(res.data.data.length > 0){
                            setNotas(res.data.data);
                        }else{
                            props.setMsg("Nenhum campo foi identificado como nota.");
                            props.setComando("");
                        }
                    }); 
                }
            });
        }        
    }, [props.fonte]);

    function cancelarNormalizar(){
        props.setComando("");
    }

    const mudancaMetodo = (event) => {
        const metodo = event.target.value;
        setMostrarSegundoSelect(metodo === "1");
    }

    function identificaSelecionados(){
        const itensSelecionados = [];
        const metodo = document.getElementById("metodo");
        const decimais = document.getElementById("decimais");
        let erro = false;
        let formData = new FormData();
        formData.append('fonte_id', fonte_id);

        if (metodo.value !== ""){
            formData.append('metodo',  parseInt(metodo.value));

            if (metodo.value === "1"){
                if (decimais && decimais.value === "") {
                    setErroInterno("O número de casa decimais não foi selecionado");
                    erro = true;    
                }else{
                    formData.append('decimais',  parseInt(decimais.value));
                }        
            }

            notas.forEach((nota, index) => {
                const checkbox = document.getElementById("nota_" + index);
                if (checkbox.checked) {
                    itensSelecionados.push(index);
                }
            });

            if (itensSelecionados.length > 0){
                if (!erro){
                    formData.append('notas', JSON.stringify(itensSelecionados));
                
                    props.client.post(
                        "/normalizar/normaliza_nota",
                        formData,
                        config
                    ).then(function(res) {
                        props.setComando("");
                    });
                }                
            }else{
                setErroInterno("Nenhuma feature foi selecionada para normalizar.");
            }
        }else{
            setErroInterno("O método de normalização não foi selecionado");
        }  
    }

    return(
        <>
        <div className="fontes">
            <button className="tab-join" onClick={cancelarNormalizar}>x</button>
            <p className="detalhes_titulo">Normalizar Nota</p>
            <br />
            {tipo === 0 ?(
            <div className="row mb-3">
                <div className="row col">
                    <div className="col-md-4"><label>Banco: </label></div>
                    <div className="col-md-8"><input type="text" className="form-control" value={banco} disabled/></div>
                </div>
                <div className="row col">
                    <div className="col-md-4"><label>SGBD: </label></div>
                    <div className="col-md-8"><input type="text" className="form-control" value={tecnologia[tipo][tecnologia_local]?.nome} disabled/></div>
                </div>
            </div>
            ):(
            <div className="row mb-3">
                <div className="row col">
                    <div className="col-md-4"><label>Arquivo: </label></div>
                    <div className="col-md-8"><input type="text"  className="form-control" value={banco} disabled/></div>
                </div>
                <div className="row col">
                    <div className="col-md-4"><label>Extensão: </label></div>
                    <div className="col-md-8"><input type="text" className="form-control" value={tecnologia[tipo][tecnologia_local]?.nome} disabled/></div>
                </div>
            </div>
            )}
            <p className="normalizacao_header"><strong>Amostra dos dados</strong></p>
            {props.fonte.id !== 0 && dataFrame.length > 0 &&(
            <RenderizaTabela
                fonte={props.fonte}
                selecionou={selecionou}
                dataFrame={dataFrame}
                setMsg={props.setMsg}
                setComando={props.setComando}
            />
            )}
            {notas.length > 0 &&(
            <>
                <p className="normalizacao_header"><strong>Seleção de features</strong></p>
                {erroIntero &&(
                    <AlertaErro msg={erroIntero} setMsg={setErroInterno}/>
                )}
                <div className="normalizar_checkField">
                    {notas.map((nota, index) => (
                    <div key={index} className="nota_item">
                        <div className="item_content">
                        <label className="col">{nota}</label>
                        <div className="form-switch">
                            <input className="form-check-input" type="checkbox" id={"nota_" + index} />
                        </div>
                        </div>
                    </div>
                    ))}
                </div>
                <div className="normalizar_metodos">
                    <p>Método</p>
                    <select onChange={mudancaMetodo} className="form-select metodo_item" aria-label="Default select example" id="metodo">
                      <option value="">Selecione um método.</option>
                      <option value="0">Conversão para Letras</option>
                      <option value="1">Conversão para Numeral</option>
                    </select>
                    {mostrarSegundoSelect && (
                    <select className="form-select metodo_item" aria-label="Default select example" id="decimais">
                        <option value="">Quantidade de casas decimais.</option>
                        <option value="0">Inteiros.</option>
                        <option value="1">Uma casa decimal.</option>
                        <option value="2">Duas casas decimais.</option>
                    </select>
                )}
                </div>
                <button onClick={identificaSelecionados} className="btn btn-primary">Normalizar</button>
            </>
            )}
        </div>
        </>
    );
}

function RenderizaTabela({fonte, selecionou, dataFrame, setMsg, setComando}){
    const [headers, setHeaders] = useState([]);
    const [dataRows, setDataRows] = useState([]);

    useEffect(() => {
        if (fonte.id == 0){
            setMsg("Nenhuma fonte selecionada.");
            setComando("");
        }else if (fonte.tipo === 0 && !selecionou){
            setMsg("As features desta fonte de dados ainda não foram selecionadas.");
            setComando("");
        }else if (dataFrame.length === 0){
            setMsg("Erro ao obter os dados.");
            setComando("");
        }else{
            setHeaders(dataFrame[0]);
            setDataRows(dataFrame.slice(1, 11));
            setMsg("");
        }        
    }, [dataFrame, fonte, selecionou, setMsg, setComando])
    
    if (dataFrame.length === 0) {
        return null; // ou uma mensagem de carregamento, etc.
    }

    return (
        <table className="table">
            <thead>
                <tr>
                    {headers.map((header, index) => (
                        <th key={index}>{header}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {dataRows.map((linha, linhaIndex) => (
                    <tr key={linhaIndex}>
                        {linha.map((celula, celulaIndex) => (
                            <td key={celulaIndex}>{celula}</td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    );
};