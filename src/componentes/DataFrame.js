import React, { useRef, useEffect, useState} from 'react';
import ReactPaginate from 'react-paginate';
import { ContextMenu } from 'primereact/contextmenu';
import Carregando from './Carregando'
import 'primereact/resources/themes/arya-blue/theme.css';
import 'primeicons/primeicons.css';

export default function Tabela(props){
  const [pageNumber, setPageNumber] = useState(0);
  const [paginateKey, setPaginateKey] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(10); 
  const [columnNames, setColumnNames] = useState([]); 
  const [editingColumnIndex, setEditingColumnIndex] = useState(null);
  const [editingCellIndex, setEditingCellIndex] = useState(null);
  const [selectedColumnIndex, setSelectedColumnIndex] = useState(null);
  const [selectedRowIndex, setSelectedRowIndex] = useState(null);
  const [cellValue, setCellValue] = useState(null);
  const [valor, setValor] = useState(null);
  
  const pageCount = Math.ceil((props.dataFrame.length - 1) / itemsPerPage);
  const offset = pageNumber * itemsPerPage;
  const currentPageData = props.dataFrame.slice(offset + 1, offset + itemsPerPage + 1); // Ignora a primeira linha que é o cabeçalho
  const config = {
    headers: {
        'X-CSRFToken': props.csrfToken,
    }
  }

  useEffect(() => {
    setPageNumber(0);
    setPaginateKey(prevKey => prevKey + 1); 
  }, [props.fonte_id]);

  useEffect(() => {
    setColumnNames(props.dataFrame[0]);

    if (props.reload){
      props.setReload(false);

      props.client.get(
        "/pincel/alteracao/"+props.fonte_id+"/",
        config
      ).then(function(res) {
        let dataFrame = JSON.parse(JSON.stringify(props.dataFrame));
        res.data.data.forEach(item => {
          if (item.tipo === 3){
            let valor = JSON.parse(item.valor);
            if (valor[0] === 0){
              dataFrame = removerColuna(dataFrame, valor[1]);
            }else if (valor[0] === 1){ //Remover Linha
              dataFrame = dataFrame.filter((_, index) => index !== valor[1]);
            }
          }
          if (item.tipo === 2){
            let valor = JSON.parse(item.valor);
            if (valor[0] === 0){
              dataFrame[0][valor[1]] = valor[2];
            }else{
              dataFrame[valor[1]][valor[2]] = valor[3];
            }
          }
        });
        props.setDataFrame(dataFrame);
      }).catch(error => {
          if (error.response) {
          console.log('Erro de resposta do servidor:', error.response.data);
          } else if (error.request) {
          console.log('Sem resposta do servidor:', error.request);
          } else {
          console.log('Erro ao processar a solicitação:', error.message);
          }
      });
    }    
  }, [props.dataFrame]);

  const handlePageClick = ({ selected }) => {
    setPageNumber(selected);
    setEditingCellIndex(null);
    setEditingColumnIndex(null);
  };

  const handleItemsPerPageChange = (e) => {
    setItemsPerPage(parseInt(e.target.value));
    setPageNumber(0); // Reiniciar para a primeira página quando a quantidade de itens por página é alterada
  };

  const handleColumnNameDoubleClick = (index) => {
    cancelEditing(index);
    setEditingColumnIndex(index);
    setEditingCellIndex(null);
  };

  const handleCellDoubleClick = (rowIndex, cellIndex, cellValue) => {
    cancelEditing(cellIndex, rowIndex);
    setEditingCellIndex(rowIndex);
    setEditingColumnIndex(cellIndex);
    setCellValue(cellValue);
  };
  
  const cancelEditing = (cellIndex, rowIndex) => {
    if (rowIndex == null){
      setValor(props.dataFrame[0][cellIndex]);
    }
    
    if (editingColumnIndex!=null || editingCellIndex!=null){
      if (editingCellIndex == null){
        const newColumnNames = [...columnNames];
        newColumnNames[editingColumnIndex] = valor;
        setColumnNames(newColumnNames);
      }
    }
  }
  
  const handleColumnNameChange = (e, index) => {
    const newColumnNames = [...columnNames];
    newColumnNames[index] = e.target.value;
    setColumnNames(newColumnNames);
  };
  
  const handleCellChange = (e) => {
    setCellValue(e.target.value);
  };

  const handleColumnNameKeyPress = (e) => {
    if (e.key === 'Enter') {
      const newDataFrame = [...props.dataFrame];
      
      if (columnNames[editingColumnIndex] !== props.dataFrame[0][editingColumnIndex]){
        gravaAlteracao(props.csrfToken, props.client, 2, JSON.stringify([0,editingColumnIndex, columnNames[editingColumnIndex]]), props.fonte_id);
      }
      
      newDataFrame[0][editingColumnIndex] = columnNames[editingColumnIndex];
      props.setDataFrame(newDataFrame);
      setEditingColumnIndex(null);
    }
  };

  const handleCellKeyPress = (e) => {
    if (e.key === 'Enter') {
      const index = (pageNumber*itemsPerPage)+editingCellIndex+1

      if (cellValue !== props.dataFrame[index][editingColumnIndex]){
        gravaAlteracao(props.csrfToken, props.client, 2, JSON.stringify([1,editingCellIndex+1, editingColumnIndex, cellValue]), props.fonte_id);
      }

      props.dataFrame[index][editingColumnIndex] = cellValue;
      setEditingCellIndex(null);
      setEditingColumnIndex(null);
    }
  };

  const handleContextMenu = (e, columnIndex) => {
    e.preventDefault();
    cm.current.show(e);
    rowContext.current.hide();
    setSelectedColumnIndex(columnIndex);
  };

  const handleRowContextMenu = (e, rowIndex) => {
    e.preventDefault();
    rowContext.current.show(e);
    cm.current.hide();
    setSelectedRowIndex(rowIndex);
  };

  const handleDeleteColumn = () => {
    props.setDataFrame(removerColuna(props.dataFrame, selectedColumnIndex));
    gravaAlteracao(props.csrfToken, props.client, 3, JSON.stringify([0,selectedColumnIndex]), props.fonte_id);
  };

  const handleDeleteRow = () => {
    props.setDataFrame(removerLinha(props.dataFrame, selectedRowIndex, pageNumber, itemsPerPage));
    gravaAlteracao(props.csrfToken, props.client, 3, JSON.stringify([1,(pageNumber*itemsPerPage)+selectedRowIndex+1]), props.fonte_id);
  };

  const cm = useRef(null);
  const rowContext = useRef(null);
  const items = [{
                    label: 'Deletar Coluna',
                    icon: 'pi pi-fw pi-trash',
                    command: handleDeleteColumn
                  },]
  const itemsMenuRow = [{
                    label: 'Deletar Linha',
                    icon: 'pi pi-fw pi-trash',
                    command: handleDeleteRow
                  },]
  
  return (
    <div>
      <div className="row mb-3 col-sm-6">
        <label htmlFor="itemsPerPage" className="col-sm-3 col-form-label">Itens por página: </label>
        <div className="col-sm-3">
          <select id="itemsPerPage" value={itemsPerPage} onChange={handleItemsPerPageChange} className="form-select">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
          </select>
        </div>
      </div>
      <table className="table table-hover table-scroll" onContextMenu={(e) => e.preventDefault()}>
        <thead>
          <tr>
            {columnNames.map((header, index) => (
              <th key={index} 
                onDoubleClick={() => handleColumnNameDoubleClick(index)}
                onContextMenu={(e) => handleContextMenu(e, index)}
              >
                {editingColumnIndex === index && editingCellIndex === null ? (
                  <input
                    type="text"
                    value={header}
                    onChange={(e) => handleColumnNameChange(e, index)}
                    onKeyDown={handleColumnNameKeyPress}
                  />
                ) : (
                  header
                )}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {currentPageData.map((row, rowIndex) => (
            <tr key={rowIndex}>
              {row.map((cell, cellIndex) => (
                <td key={cellIndex}
                  onContextMenu={(e) => handleRowContextMenu(e, rowIndex)}
                  onDoubleClick={() => handleCellDoubleClick(rowIndex, cellIndex, cell)}
                >
                {editingCellIndex === rowIndex && editingColumnIndex === cellIndex ? (
                  <input
                    type="text"
                    value={cellValue}
                    onChange={(e) => handleCellChange(e)}
                    onKeyDown={handleCellKeyPress}
                  />
                ) : (
                  cell
                )}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      <div className="pagination-container">
        <nav aria-label="Paginação da tabela">
          <ReactPaginate
            key={paginateKey}
            previousLabel={'Anterior'}
            nextLabel={'Próximo'}
            pageCount={pageCount}
            onPageChange={handlePageClick}
            containerClassName={'pagination'}
            activeClassName={'active'}
            disabledClassName={'disabled'}
            disabledLinkClassName={'page-link'}
            previousClassName={'page-item'}
            previousLinkClassName={'page-link'}
            nextClassName={'page-item'}
            nextLinkClassName={'page-link'}
            pageClassName={'page-item'}
            pageLinkClassName={'page-link'}
            breakClassName={'page-item'}
            breakLinkClassName={'page-link'}
          />
        </nav>
        <div className="pagination-info">
          Apresentado de {offset + 1} a {Math.min(offset + itemsPerPage, props.dataFrame.length - 1)} de {props.dataFrame.length - 1} valores
        </div>
      </div>
      <div>
        <ContextMenu
          model={items}
          ref={cm}
          breakpoint="767px"
          pt={{
              action: ({ props, state, context }) => ({ className: context.active ? 'bg-primary-200' : undefined })
          }}
        />
        <ContextMenu
          model={itemsMenuRow}
          ref={rowContext}
          breakpoint="767px"
          pt={{
              action: ({ props, state, context }) => ({ className: context.active ? 'bg-primary-200' : undefined })
          }}
        />
      </div>
    </div>
  );
}

export function removerColuna(listaDeListas, colunaIndex){
    return listaDeListas.map(item => {
      return item.filter((_, index) => index !== colunaIndex);
    })
}

export function removerLinha(listaDelistas, linhaIndex, pageNumber, itemsPerPage){
  const linha = (pageNumber*itemsPerPage)+linhaIndex+1
  const lista = listaDelistas.filter((_, index) => index !== linha);
  return lista;
}

function gravaAlteracao(csrfToken, client, tipo, valor, fonte_id){
  const config = {
    headers: {
        'X-CSRFToken': csrfToken,
    }
  }
  
  const formData = new FormData();
  formData.append('fonte_id', fonte_id);
  formData.append('tipo', tipo);
  formData.append('valor', valor);

  client.put(
      "/pincel/alteracao",
      formData,
      config
  ).then(function(res) {
    console.log(res.data.data);
  }).catch(error => {
      if (error.response) {
      console.log('Erro de resposta do servidor:', error.response.data);
      } else if (error.request) {
      console.log('Sem resposta do servidor:', error.request);
      } else {
      console.log('Erro ao processar a solicitação:', error.message);
      }
  });
}