import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Login from './componentes/Login';
import Navbar from './componentes/Navbar';
import Sidebar from './componentes/Sidebar'
import ModalConectar, { ModalNovoProjeto, ModalBanco } from './componentes/Modal';
import Projeto, {SelecionaProjetos} from './componentes/Projetos';
import Carregando from './componentes/Carregando';

const client = axios.create({
  baseURL: "http://127.0.0.1:8000",
  withCredentials: true
});

function App() {
  const [registrationToggle, setRegistrationToggle] = useState(false);
  const [currentUser, setCurrentUser] = useState();
  const [user, setUser] = useState([]);
  const [projetoAtivo, setProjetoAtivo] = useState(false);
  const [projeto, setProjeto] = useState({id: '', nome: '', descricao: ''});
  const [selecionando, setSelecionando] = useState('');  
  const [csrfToken, setCsrfToken] = useState('');
  const [conexoes, setConexoes] = useState([]);
  const [carregando, setCarregando] = useState(false);
  const [etapa, setEtapa] = useState(0);
  const [join, setJoin] = useState(false);
  const [comando, setComando] = useState('');

  useEffect(() => {
    client.get("/api/user")
    .then(function (res) {
      if(res.data.data){
        setCurrentUser(res.data.data);
        setUser(res.data.usuario);          
        
        const storedData = sessionStorage.getItem('projeto');

        if (storedData) {
          const projeto_sessao = JSON.parse(storedData);
          setProjeto(projeto_sessao);
          setProjetoAtivo(true)
        }
      }else{
        setCurrentUser(res.data.data);
      }
      setCsrfToken(res.data.csrftoken)
    })
    .catch(function (error) {
      setCurrentUser(false);
    });
  },[currentUser]); 

  function limpaVariaveis(){
    setRegistrationToggle(false);
    setCurrentUser();
    setUser([]);
    setProjetoAtivo(false);
    setProjeto({id: '', nome: '', descricao: ''});
    setSelecionando('');  
    setCsrfToken('');
  }

  return (
    <div>
      <Navbar 
        csrfToken={csrfToken}
        registrationToggle={registrationToggle}
        client={client} 
        user={user} 
        currentUser={currentUser} 
        limpaVariaveis={limpaVariaveis}
        setRegistrationToggle={setRegistrationToggle}
        setCurrentUser={setCurrentUser}
        setSelecionando={setSelecionando} 
      />
      <Sidebar 
        currentUser={currentUser} 
        projetoAtivo={projetoAtivo}
        etapa={etapa}
        setJoin={setJoin}
        setComando={setComando}
      />
      {currentUser ?(
        projetoAtivo && selecionando !== 'Projeto' ?(
          <div>
            <Projeto  
              csrfToken={csrfToken} 
              client={client} 
              conexoes={conexoes}
              projeto={projeto}
              etapa={etapa}
              join={join}
              comando={comando}
              setConexoes={setConexoes}
              setProjeto={setProjeto} 
              setProjetoAtivo={setProjetoAtivo}
              setSelecionando={setSelecionando}
              setCarregando={setCarregando}
              setEtapa={setEtapa}
              setJoin={setJoin}
              setComando={setComando}
            />
            <ModalConectar
              csrfToken={csrfToken}
              client={client}
              projeto={projeto}
              setSelecionando={setSelecionando}
              setConexoes={setConexoes}
              setCarregando={setCarregando}
            />
          </div>
        ):(
          selecionando === 'Projeto' &&(
            <SelecionaProjetos csrfToken={csrfToken} client={client} setProjeto={setProjeto} setProjetoAtivo={setProjetoAtivo} setSelecionando={setSelecionando} />
          )
        )
      ):(
        <Login 
          csrfToken={csrfToken}
          registrationToggle={registrationToggle}
          setRegistrationToggle={setRegistrationToggle}
          currentUser={currentUser}
          setCurrentUser={setCurrentUser}
          client={client}
        />
      )}
      {currentUser && !projetoAtivo &&(
        <ModalNovoProjeto csrfToken={csrfToken} client={client} setProjetoAtivo={setProjetoAtivo} setProjeto={setProjeto} setSelecionando={setSelecionando} />
      )}
      {selecionando === 'Banco' &&(
        <ModalBanco csrfToken={csrfToken} client={client} setConexoes={setConexoes} setSelecionando={setSelecionando} />
      )}
      {carregando &&(
        <Carregando />
      )}
    </div>
  );
}

export default App;
